��    x     �              �     �     �     �     �     �     �     �     �     �  �    R   �  x        ~     �     �     �     �  �   �  H   �     "     1     8     @     D     Q     a     r  F   �     �  	   �     �       #   "  %   F     l     {     �     �     �     �     �     �     �     �     �     �                 �     &   �        a   '      �   #   �   #   �   �   �   �   �!  �   @"  �   �"     �#  	   �#     �#     �#     �#     �#     �#     �#  +   $  2   E$     x$     �$     �$     �$     �$     �$     �$     %     %     4%     M%     \%     k%     }%  	   �%     �%  )   �%     �%     �%  )   &     B&     X&     x&  ,   �&     �&     �&  	   �&     �&     '     '     :'     L'     S'     a'     s'     �'     �'     �'     �'     �'     �'     (     (     (  	   2(     <(     B(     [(  <   r(     �(  
   �(     �(     �(     �(     �(  
   �(     
)     )  	   +)     5)     F)     V)     g)     l)  	   u)     )     �)     �)  	   �)     �)     �)  
   �)     �)     �)     �)     �)  "   *     +*  i   9*  l   �*     +     #+     =+     U+     c+  !   h+     �+     �+     �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     ,     ,     ,     .,     2,     E,     Y,     ^,     j,     v,     y,     �,  &   �,     �,  '   �,     �,     -     +-     =-     B-     a-     o-     }-     �-     �-  #   �-     �-     �-  	   �-     .     .  /   -.     ].  
   c.     n.     z.     �.     �.     �.     �.     �.     �.     �.     �.     �.     �.     /     !/     2/     M/     T/  
   [/     f/     n/     /     �/     �/     �/     �/     �/     �/  
   �/     �/     �/     
0     0     -0     40     K0     a0     q0     �0     �0     �0     �0  	   �0     �0     �0     �0     1     1     ,1  >   <1     {1  	   �1     �1     �1     �1     �1     �1     �1  �   �1  I   u2  =   �2  }   �2  �   {3  6   	4  !   @4  �   b4  6   �4     %5  4   *5     _5     c5     v5     z5     �5     �5     �5     �5     �5     �5     6     '6     /6     H6  +   Y6  
   �6     �6  	   �6     �6  '   �6      �6     �6     �6  (   7     .7  .   ;7  /   j7  "   �7     �7     �7     �7  J   �7  P   )8  k  z8     �9  -   :  %   3:  7   Y:     �:  	   �:     �:     �:     �:     �:  
   �:     �:     �:  
   �:     ;     ;  
   ,;  	   7;     A;     S;     c;     i;     o;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     <     <     <     $<     )<     2<  	   B<  	   L<     V<     _<     q<     y<     �<     �<     �<     �<     �<  
   �<     �<     �<     �<     �<     �<     =     =     =      =     2=     4=     R=  <   j=     �=     �=     �=     �=     �=  �  �=     �?     �?     �?     �?     �?     @     "@     $@     '@  {  8@  T   �B  �   	C     �C     �C     �C     �C     �C    D  Q   E     eE     vE     |E     �E     �E     �E     �E     �E  O   �E     F     1F     >F     YF  #   jF  $   �F     �F     �F  	   �F     �F  	   �F     �F  "   G     )G     2G     9G     LG     PG  	   ^G     hG     qG  �   zG  '   _H     �H  j   �H     I  5   I  *   LI  �   wI     J  �   �J  �   _K     L     L     (L  	   8L     BL     ^L  	   wL     �L  >   �L  ;   �L     M  $   5M  #   ZM     ~M     �M     �M     �M     �M     �M     �M     N  	   #N     -N     =N     VN  -   ^N  1   �N  '   �N  "   �N  4   	O  '   >O  (   fO     �O  2   �O  (   �O  #    P     $P     5P     CP  $   XP     }P     �P     �P     �P     �P      �P     �P     Q     Q     (Q     FQ     YQ     mQ     uQ  
   �Q     �Q  '   �Q  !   �Q  B   �Q     3R     <R     DR     QR  #   ZR     ~R     �R     �R     �R     �R     �R     �R     �R     S     S     'S  	   4S  *   >S  
   iS  
   tS     S     �S     �S     �S     �S     �S     �S  !   �S     �S  r   T  r   yT     �T     U     U     .U     <U     AU     \U  	   eU     oU     �U     �U     �U     �U     �U     �U     �U     �U     	V     V     V     V     'V     +V      EV  	   fV     pV     �V     �V     �V     �V     �V     �V  <   �V     (W     DW     [W     oW  &   uW     �W  
   �W     �W     �W     �W  +   �W     X     X     +X     2X     GX  0   `X     �X  
   �X  
   �X  	   �X     �X     �X     �X     �X     �X     Y     Y     $Y     9Y     AY     QY     hY     zY     �Y     �Y     �Y  	   �Y     �Y     �Y     �Y  #   �Y     Z     "Z     /Z  
   6Z     AZ     OZ     \Z     uZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z     
[     [     -[     :[     A[     Y[     p[  
   �[  !   �[     �[  ?   �[  
   \     \     +\     3\     <\     L\     ^\     s\  �   �\  I   ,]  A   v]  w   �]  �   0^  5   �^  #   �^  z   #_  0   �_     �_  5   �_     `     `     #`     *`     9`     B`     R`      m`  +   �`  /   �`     �`     �`     a     &a  '   @a     ha  
   za     �a     �a  -   �a     �a     �a  	   �a  $   b     *b  B   9b  B   |b  *   �b     �b     c     c  L   c  _   Tc  s  �c     (e  (   >e  -   ge  /   �e     �e     �e  
   �e     �e     �e  
   f     f      f  	   ,f     6f     Bf     Tf     tf     }f     �f     �f  	   �f     �f     �f     �f     �f     �f     �f     �f     g     .g     Dg     Sg     ag     ig     ng  
   ~g     �g  
   �g     �g  
   �g     �g  
   �g  
   �g  	   �g     �g     �g     h  	   !h     +h     =h     Eh     Wh     hh     th  	   �h     �h     �h     �h  
   �h     �h     �h     �h     �h  ,   �h     i  ?   8i     xi  !   �i     �i     �i     �i    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : :  <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 <b>Write Concurrency Warning:</b>

This record has been modified while you were editing it.
 Choose:
    - "Cancel" to cancel saving;
    - "Compare" to see the modified version;
    - "Write Anyway" to save your current version. A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Shortcut Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Could not connect to the server Create Create a new record Create new database Create new line Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Fetching databases list Field Separator: Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Host: Hostname: Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Incompatible version of the server Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit Limit: Lines to Skip: Link List Entries Shortcuts Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open relation Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select a File... Select an Image... Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To report bugs you must have an account on <u>%s</u> To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Undelete selected record Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: Username: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Reload _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version: tryton 1.8
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2010-12-31 20:43+0100
PO-Revision-Date: 2011-10-24 01:07+0200
Last-Translator: Venceslav Vezjak <vezjakv@gmail.com>
Language-Team: sl_SI <LL@li.org>
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  od  %d zapis uvožen! %d zapis shranjen! %d zapisov uvoženih! %d zapisov shranjenih! %s datoteka : :  <b>Vsa polja</b> <b>Ali poznate Triton, enega od soimenjakov našega projekta?</b>

Triton (angleška izgovorjava /ˈtraɪtən/ TRYE-tən, oziroma v grščini
Τρίτων) je največja luna planeta Neptun, ki jo 10.oktobra, 1846,
odkril William Lassell. Je edina ogromna luna v sončnem sistemu z
retrogradno orbito, t.j. orbito v nasprotni smeri kroženja njenega
planeta. S premerom 2.700 km je sedma največja luna v sončnem
sistemu. Triton predstavlja več kot 99,5 odstotkov vse mase v orbiti
Neptuna, vključno s planetnimi kolobarji in ostalimi 12 znanimi
lunami. Je tudi bolj masiven od skupaj vseh 159 znanih manjših lun v
sončnem sistemu. <b>Izvozi grafe</b>

Z desnim klikom lahko shranite katerikoli graf v PNG datoteko.
 <b>Izvozi naštete zapise</b>

Iz kateregakoli seznama lahko kopirate zapise z Ctrl + C
in jih prilepite v katerokoli aplikacijo z Ctrl + V
 <b>Polja za izvoz</b> <b>Polja za uvoz</b> <b>Možnosti</b> <b>Predefinirani izvozi</b> <b>Dobrodosli v Tryton</b>


 <b>Opozorilo sočasnega zapisovanja:</b>

Ta zapis je bil spremenjen med vašim popravljanjem.
 Izberite:
    - "Prekliči" za preklic shranjevanja;
    - "Primerjaj" za vpogled v spremenjeno inačico;
    - "Vseeno zapiši" za shranitev vaše trenutne inačice. Zbirke podatkov s tem imenom že obstaja.
Poskusi z novim imenom zbirke podatkov. Dostop zavrnjen! Ukrep Ukrepi Dodaj Dodaj bližnjico Dodaj prevod Dodaj imena _polj Dodaj priponko zapisu Skrbniško geslo in potrditev gesla sta potrebna za stvarjenje zbirke podatkov. Skrbniško geslo: Vse datoteke Vedno prezri to opozorilo. Napaka programa! Ali res želiš izbrisati ta zapis? Ali res želiš izbrisati te zapise? Priponka(%d) Priponka(0) Priponka: Samozaznava Arhiviraj Arhiviraj zbirke podatkov Arhiviraj izbrano zbirko podatkov. Vsebina: Spodaj Sledilec hroščev Kp: CSV parametri _Spremeni P_oveži _Ustvari Ni možno ustvariti zbirke podatkov zaradi neznanega razloga.
Če je zbirka podatkov ustvarjena, je verjetno pokvarjena in jo mogoče zbriši! Prosimo, preveri sporočilo napake za možne dodatne informacije.
Sporočilo napake:
 Prekliči povezavo do Tryton strežnika Spremeni bližnjice Po obnovitvi zbirke podatkov iz prejšnje Tryton inačice preveri samodejno posodabljanje zbirke podatkov. Izberi vtičnik Izberi Tryton zbirko podatkov za varnostno kopiranje: Za brisanje izberi Tryton zbirko podatkov: Izberi geslo za skrbnika nove zbirke podatkov. S to poverilnico se lahko kasneje prijaviš v zbirko podatkov:
Uporabniško ime: admin
Geslo: <tu nastavljeno geslo> Izberi privzet jezik, ki bo nameščen za to zbirko podatkov. Po namestitvi lahko nove jezike dodate preko skrbniškega menija. Izberi ime zbirke podatkov za obnovitev.
Dovoljeni so alfanumerični znaki oziroma "_" (podčrtaj). 
Izogibaj se vsem znakom za naglaševanje, presledku ali drugim posebnim znakom.
Primer: tryton Izberi ime nove zbirke podatkov.
Dovoljeni so alfanumerični znaki oziroma _ (podčrtaj).
Izogibaj se vsem znakom za naglaševanje, presledku ali posebnim znakom! Primer: tryton Počisti Zapri zavihek Ukazna vrstica: Primerjaj Izjema sočasnega izvajanja Potrdi skrbniško geslo: Potrditev Poveži se s Tryton strežnikom Napaka pri povezavovanju!
Napačno uporabniško ime ali geslo! Napaka pri povezovanju!
Ni se možno povezati na strežnik! Kopiraj izbrano besedilo Ni se možno povezati s strežnikom! Ni se možno povezati s strežnikom Ustvari Ustvari nov zapis Ustvari novo zbirko podatkov Ustvari novo postavko Ustvari novo vezo Ustvari novo zbirko podatkov. Ustvarjen nov hrošč z ID  Ustvarjeno: Ustvaril: Trenutno geslo: Izreži izbrano besedilo _Zbirka Varnostno kopiranje zbirke podatkov uspešno! Brisanje zbirke podatkov je odpovedalo z napako:
 Brisanje zbirke podatkov je odpovedalo! Brisanje zbirke podatkov uspešno! Zapisovanje zbirke podatkov je odpovedalo z napako:
 Zapisovanje zbirke podatkov odpovedalo! Zbirka podatkov je zaščitena z geslom! Ime zbirke podatkov: Obnovitev zbirke podatkov je odpovedala z napako:
 Obnovitev zbirke podatkov je odpovedala! Obnovitev zbirke podatkov uspešna! Zbirka podatkov: Izbira datuma Izbira datuma in ure Bližnjice za vnašanje datuma/časa Privzet jezik: Zbriši Izbriši izvoz Zbriši zbirko podatkov Izbriši izbran zapis Zbriši izbrano zbirko podatkov. Zav_rži zbirko... E-Mail report Uredi ukrepe datotek Uredi uporabniške nastavitve Uredi izbran zapis Urejevalni gradniki Epošta Nastavitve poštnega odjemalca Kodiranje: Napaka Napaka pri ustvarjanju zbirke podatkov! Napaka pri odpiranju CSV datoteke Napaka pri poskusu uvoza tega zapisa:
%s
Sporočilo napake:
%s

%s Napaka:  Izjema: Izvozi v CSV Napačno Pridobivanje seznama zbirk podatkov Ločevalec polj: Naziv polja Datoteke "%s" ni moč najti Ukrepi datotek Vrsta datoteke _Ukrepi za datoteke... Datoteka za uvoz: Datoteka za obnovitev: Najdi Pojdi na ID Pojdi na ID: Velikost: Informacija o gostitelju / zbirki podatkov Gostitelj: Gostitelj: Ura: ID: Velikost slike Slika je prevelika! Slike Uvozi iz CSV Napaka uvažanja! Nezdružljiva inačica strežnika Neveljaven obrazec! Ni možno zapisati zbirke podatkov, zaščitene z geslom.
Varnostno kopiranje in obnovitev se mora ročno izvesti. Ni možno obnoviti zbirke podatkov, zaščitene z geslom.
Varnostno kopiranje in obnovitev se mora ročno izvesti. Bližnjice na tipkovnici Nazadnje popravljeno: Nazadnje popravil: Launch action Levo Opis možnih prostornikov: Omejitev Omejitev: Izpuščene vrstice: Povezava Bližnjice za vnašanje postavk Prijava M Upravljaj bližnjico Označi vrstico za brisanje Minuta: Manjka skrbniško geslo! Model: N Naziv Napaka mreže! New Novo ime zbirke podatkov: Namestitev nove zbirke podatkov: Naslednji Naslednji zapis Naslednji gradnik Ne Ni atributov. Nobenega ukrepa ni določenega! Za ta resurs ni vtičnika! Ni povezave! Nobene zbirke podatkov ni možno najti, morate jo ustvariti! Drugih jezikov ni na voljo! Noben zapis ni izbran! Ničesar za tiskat! Odpri Odpri varnostno kopijo za obnovitev... Odpri zapis Odpri vezo Odpri koledar Odpri... Odpri/išči vezo Operacija odpovedala!
Sporočilo napake:
%s Operacija v teku PNG slika (*.png) Geslo: Gesli se ne ujemata! Vstavi kopirano besedilo Prosimo, počakajte,
ta operacija lahko traja... Vrata: Nastavitev Nastavitve Prejšnji Prejšnji zapis Prejšnji gradnik Tisk Tiskaj potek dela Tiskaj potek dela (sestavljen) Print report Profil Urejevalnik profilov Profil: Zapis shranjen! Zapisi niso izbrisani! Zapisi izbrisani! Bližnjice za vnašanje vez Ponovno naloži Odstrani Prijavi hrošča Poročila Zahteve (%s/%s) Obnovi Obnovi zbirko podatkov Obnovi zbirko podatkov iz datoteke. Desno SSL povezava Shrani Shrani kot Shrani kot... Shrani izvoz Save Tree Expanded State Shrani širino/višino Shrani ta zapis Išči Išči / Odpri zapis Nastavitev omejitev iskanja Omejitev iskanja... Poišči zapis Varnostno tveganje Izberi datoteko... Izberi sliko... Izberi ukrep Izbira Povezava do strežnika: Nastavitev strežnika: Povezava do strežnika: Strežnik: Nastavi povezavo s strežnikom... Prikaži navadno besedilo Žal napačno geslo za Tryton strežnik. Prosim, poskusi znova. Črkovanje Statusna vrstica Zadeva: Preklopi Preklopi pogled Položaj zavihkov Ločevalec besedila: Bližnjice vnašanja besedila Ime zbirke podatkov je omejeno na alfanumerične znake in "_" (podčrtaj). Izogibaj se vsem znakom za naglaševanje, presledku ali drugim posebnim znakom. The following action requires to close all tabs.
Do you want to continue? Novo skrbniško geslo se ne ujema s tistim v potrditvenem polju.
 Drugi uporabnik je že prijavil isti hrošč.
Zaradi obveščanja je vaše uporabniško ime dodano seznam tega hrošča Prstni odtis strežnika se je spremenil od zadnje povezave!
Program bo ustavil povezovanje na ta strežnik, dokler ne bo njegov prstni odtis popravljen. Inačica tega odjemalca ni združljiva s strežnikom! To ime zbirke podatkov že obstaja! To je geslo Tryton strežnika in ni povezano s pravim uporabnikom. To geslo je ponavadi določeno v trytond konfiguraciji. Ta zapis je bil spremenjen,
ga želiš shraniti? Namigi Za prijavo hroščev morate imeti račun na <u>%s</u> Za: Preveč argumentov Zgoraj Prevedi pogled Pravilno Tryton povezava Geslo za Tryton strežnik: Ponovno vtipkaj skrbniško geslo Ni možno nastaviti krajevnih nastavitev %s Ni možno zapisati konfiguracijske datoteke %s! Povrni izbran zapis Neznano Opusti izbor vrtice za brisanje Posodobi zbirko podatkov: Kot ogrado za ime datoteke uporabi "%s" Uporabniško ime: Uporabnik: Uporabniško ime: Poglej dnevni_k... Čakajoče zahteve: %s prejetih - %s poslanih Kako se imenuje ta izvoz? Širina: Čarovnik Sedaj se dela na podvojenih zapisih! Vseeno zapiši Napačno geslo za Tryton strežnik.
Prosimo, poskusite še enkrat. Napačno geslo za Tryton strežnik.
Prosimo, poskusite še enkrat. Nedovoljeni znaki v imenu zbirke podatkov! Napačna ikona za gumb! l Da Nameravate zbrisati Tryton zbirko podatkov.
Ali ste res želite nadaljevati? Ne morete se prijaviti v sistem!
Preverite, če imate definiran meni za vaše uporabniško ime. Uporabite lahko posebne operaterje:
* + za povečanje datuma
* - za zmanjšanje datuma oziroma prazen datum
* = za nastavitev datuma oziroma trenutni datum

Na voljo so spremenljivke:
h za ure
d za dneve
w za tedne (samo z +/-)
m za mesece
y za leta

Primeri:
"+21d" povečaj za 21 dni od datuma
"=11m" nastavi datum na 11.mesec leta
"-2w"  zmanjšaj za 2 tedna od datuma Izbrati moraš zapis! Za uporabo te veze morate izbrati zapis! Najprej morate izbrati datoteke za uvažanje! Pred dodajanjem prevodov morate zapis shraniti! Vaša izbira: _Vizitka... _Ukrepi... _Dodaj _Arhiviraj zbirko... _Prekliči Zapri zavi_hek _Poveži... _Privzeto Z_briši... Pre_kini povezavo Naslednjič _prikaži nov namig _Podvoji _Epošta... _Izvedi vtičnik I_zvozi podatke... _Datoteka O_brazec Pojdi na zap_is ID... _Pomoč _Domov _Ikone Uv_ozi podatke... _Bližnjice na tipkovnici... _Upravljaj profile _Ponovno naloži meni _Preklopi meni _Menu vrstica _Način _Nov _Nova zbirka... Nas_lednji _Navaden _Možnosti _PDA _Vtičniki _Nastavitve... P_rejšnji _Tiskaj... _Izhod... P_reglej lastne zahteve _Ponovno naloži Ponovno naloži/Razvel_javi _Odstrani Po_vrni zbirko... _Shrani _Shrani možnosti _Pošlji zahtevo _Bližnjice Preklopi po_gled _Besedilo Bese_dilo in ikone _Nasveti... _Orodna vrstica _Uporabnik d u beleženje vsega na INFO ravni m določi alterntivno konfiguracijsko datoteko določi kanale za beleženje določi raven beleženja: INFO, DEBUG, WARNING, ERROR, CRITICAL določi uporabniško ime določi strežniškega gostitelja določi strežniška vrata t y 