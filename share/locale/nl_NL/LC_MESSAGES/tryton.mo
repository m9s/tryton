��    f     L              |     }     �     �     �     �     �     �     �     �  �  �  R   �  x   �     ^     v     �     �     �  H   �          +     2     :     >     N     _  F        �  	   �     �     �  #     %   3     Y     h     v     �     �     �     �     �     �     �     �     �     �     �       �   	  &   �        a        v  #   �  #   �  �   �  �   �  �   -   �   �      �!  	   �!     �!     �!     �!     �!     �!     �!  +   "  2   2"     e"     x"     �"     �"     �"     �"     �"     #     #     !#     3#  	   E#     O#  )   o#     �#     �#  )   �#     �#     $     .$  ,   =$     j$     �$  	   �$     �$     �$     �$     �$     %     	%     %     )%     @%     ]%     o%     }%     �%     �%     �%     �%     �%  	   �%     �%     �%     &  <   (&     e&  
   m&     x&     �&  
   �&     �&     �&  	   �&     �&     �&     �&     �&     �&  	   '     '     '     0'     6'  
   :'     E'     ['     b'     r'     �'  i   �'  l   �'     j(     }(     �(     �(     �(  !   �(     �(     �(     �(      )     )     )     )     )     4)     <)     T)     [)     ])     b)     q)     u)     �)     �)     �)     �)     �)     �)     �)  &   �)     *  '   *     =*     Z*     n*     �*     �*     �*     �*     �*     �*  #   �*     +     +  	   -+     7+     P+  /   b+     �+  
   �+     �+     �+     �+     �+     �+     �+     �+     ,     ,     ,     *,     3,     A,     V,     g,     �,     �,  
   �,     �,     �,     �,     �,     �,     �,     �,     -     -  
   -     -     &-     ?-     Q-     b-     i-     �-     �-     �-     �-     �-  	   �-     �-     �-     .     .     .     =.  >   M.     �.  	   �.     �.     �.     �.     �.     �.     �.  �   �.  I   �/  =   �/  }   0  �   �0  6   1  !   Q1  �   s1  6   �1     62     ;2     ?2     R2     V2     e2     j2     |2     �2     �2     �2     �2     �2     3  +   3  
   H3     S3     Y3  '   g3      �3     �3     �3  (   �3     �3  .   �3  /   #4  "   S4     v4     �4     �4  J   �4  P   �4  k  35     �6  -   �6  %   �6  7   7     J7  	   Z7     d7     p7     u7     �7  
   �7     �7     �7  
   �7     �7     �7  
   �7  	   �7     �7     8     8     "8     (8     <8     B8     H8     O8     _8     v8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8  	   �8  	   �8     9     9     9     %9     29     :9     O9     U9     c9  
   s9     ~9     �9     �9     �9     �9     �9     �9     �9      �9     �9     �9     �9  <   :     S:     j:     �:     �:     �:  �  �:     &<     ,<     C<     W<     o<  
   �<     �<     �<     �<  �  �<  W   ?  s   �?     K@     j@     �@  +   �@     �@  I   �@     )A     7A     =A  	   DA     NA     aA  !   tA  K   �A     �A     �A  !   B     %B  /   3B  1   cB     �B     �B     �B     �B     �B     �B  $   �B     C  	   C     C     -C     1C  	   @C  
   JC  	   UC  �   _C  %   FD     lD  X   �D     �D  /   �D  +   E  �   GE  �   F  �   �F  �   IG     �G     �G     H  	   H     H     .H     IH     UH  8   oH  :   �H     �H     �H     I     2I     LI     dI  "   }I     �I     �I     �I     �I  	   �I  "   �I  1   J  $   IJ     nJ  1   �J     �J  &   �J     K  0   K     DK     bK  	   �K     �K     �K     �K     �K     �K     �K     L     L     :L     YL     oL     }L     �L     �L     �L     �L     �L  	   �L     M     
M     "M  A   >M     �M     �M     �M  	   �M     �M     �M     �M     �M     �M     N     !N     :N     ?N     ON     `N     hN     �N     �N     �N     �N     �N     �N     �N     �N  i   O  j   rO     �O     �O     P     P     (P  #   .P     RP     ZP     cP  	   uP  	   P     �P     �P     �P     �P     �P     �P     �P     �P     �P     �P     �P     Q     %Q     .Q     <Q     LQ     PQ     ^Q  +   {Q     �Q  ,   �Q     �Q     R     %R     ER  %   JR     pR     |R  	   �R     �R  "   �R     �R     �R     �R      	S     *S  2   CS     vS     }S  
   �S     �S     �S     �S  	   �S     �S      �S     �S     �S     T     T     T     /T     FT     XT     lT     uT     �T  	   �T     �T  
   �T     �T     �T     �T     �T     U     U     U     *U     AU     ZU     pU     �U     �U     �U     �U     �U     �U     �U     �U     V     V     ,V     ?V     GV     _V  H   oV     �V  
   �V  
   �V     �V     �V     W     W     -W  �   HW  I   �W  G   &X  �   nX  �   Y  W   �Y     Z  �   +Z  1   �Z     �Z     [     [  	   [     #[     4[     9[     K[  !   e[     �[  )   �[     �[     �[     �[  /   \     6\  
   F\     Q\  2   d\      �\     �\  	   �\  %   �\     �\  ?   �\  ?   ?]  "   ]  !   �]     �]     �]  I   �]  d   ^  �  y^     `  B   #`  +   f`  9   �`     �`     �`  	   �`  
   �`     �`     a      a     0a  
   >a     Ia     Ya     na     �a     �a     �a     �a     �a  	   �a     �a     �a     �a     �a     b      b     ,b     =b  	   Jb     Tb     [b     bb  	   vb     �b     �b     �b     �b     �b     �b  	   �b  	   �b     �b  	   �b     �b     �b     c     c     #c     2c     Fc     Wc     dc     kc     �c  	   �c  
   �c     �c     �c      �c     �c  .   �c  $   �c  E   d     bd     �d     �d     �d     �d    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : :  <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Create a new record Create new database Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit Limit: Lines to Skip: Link Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version: Tryton 1.8
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2011-03-08 00:13+0100
PO-Revision-Date: 2011-10-24 01:06+0200
Last-Translator: Bram van der Sar <avdsar@telfort.nl>
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  van  %d item geïmporteerd! %d item opgeslagen! %d items geïmporteerd! %d items opgeslagen! %s bestand : : <b>Alle velden</b> <b>Kent u Triton, één van de naamgevers van ons project?</b>

Triton (uitgesproken als [tri-ton], of in het Greeks Τρίτων) is de grootste
maan van de planeet Neptunus, ontdekt op 10 oktober 1846 door
William Lassell. Het is de enige grote maan in het zonnestelsel met een
tegengestelde baan, dat is een baan in de tegengestelde richting ten
opzichte van de rotatie van de planeet. Met een diameter van 2.700 km, is
het de op zeven na grootste maan in het zonnestelsel. Triton vertegenwoordigd
meer dan 99,5 procent van alle nu bekende massa die rond Neptunus cirkelt,
inclusief de ringen en de twaalf nu bekende andere manen. Hij is ook omvangrijker
dan de 159 nu bekende kleinere manen van het zonnestelsel bij elkaar.
 <b>Exporteer grafieken</b>

Met een rechts-klik kan je elke grafiek in PNG exporteren.
 <b>Exporteer gegevens</b>

Kopieer gegevens van elke lijst met Ctrl + C
en plak ze in elke toepassing met Ctrl + V
 <b>Velden om te exporteren</b> <b>Velden om te importeren</b> <b>Opties</b> <b>Voorgedefinieerde exportinstellingen</b> <b>Welkom bij Tryton</b>


 Een database met deze naam bestaat al.
Probeer een andere database naam.  Geen toegang! Actie Acties Toevoegen Voeg vertaling toe Voeg veldnamen toe Voeg een bijlage aan dit item toe Admin wachtwoord en bevestiging zijn nodig om een nieuwe database te maken. Admin wachtwoord: Alle bestanden Altijd deze waarschuwing negeren. Programmafout Weet je zeker dat je dit item wilt verwijderen? Weet je zeker dat je deze items wilt verwijderen? Bijlage (%d) Bijlage Bijlage: Automatisch Reservekopie Reservekopie van database Maak reservekopie van deze database. Bericht: Onderkant Foutrapportage CC: CSV parameters _Wijzigen _Verbinden Aan_maken Kan de database niet aanmaken als gevolg van een onbekende redenen.
Als er een database is gemaakt kan deze beschadigd zijn. Misschien moet je deze database verwijderen! Zie foutmelding voor mogelijk meer informatie.
Foutmelding:
 Verbreek verbinding met Tryton server Versnellers aanpassen Inschakelen voor het automatisch bijwerken van de database uit een vorige Tryton versie. Kies een plugin Kies een Tryton database voor een reservekopie: Kies een Tryton database om te verwijderen: Kies een wachtwoord voor de admin gebruiker van de nieuwe database. Hiermee kan je later inloggen op deze database:
Gebruikersnaam: admin
Wachtwoord: <het wachtwoord dat je hier ingeeft> Kies de standaardtaal welke geïnstalleerd wordt voor deze database. Het is mogelijk de taal te wijzigen na installatie in het Systeembeheer menu. Kies de naam van de database die hersteld moet worden.
Toegestane tekens zijn alfa-numeriek of _ (underscore)
Vermijd alle accenten, spaties en speciale tekens!
Voorbeeld: tryton  Kies de naam van de nieuwe database.
Toegestane tekens zijn alfa-numeriek of _ (underscore).
Vermijd alle accenten, spaties en speciale tekens! Voorbeeld: tryton. Wissen Tabblad sluiten Opdrachtregel: Vergelijk Simultaan gebruik Bevestig admin wachtwoord: Bevestiging Verbind met Tryton server Geen verbinding!
Verkeerde gebruikersnaam of wachtwoord! Verbinding mislukt!
Verbinden met de server niet mogelijk! Kopieer geselecteerde tekst Geen verbinding met de server! Maak een nieuw item Maak een nieuwe Database. Nieuwe relatie aanmaken Maak de nieuwe Database. Nieuwe foutmelding gemaakt met ID  Aanmaakdatum Aangemaakt door: Huidig Wachtwoord: Knip geselecteerde tekst Data_base Reservekopie van database gemaakt! Verwijderen database is mislukt met foutmelding:
 Database kan niet worden verwijderd. Database succesvol verwijderd! Verwijderen database is mislukt met foutmelding:
 Database verwijderen mislukt! Database beveiligd met een wachtwoord! Naam database: Herstellen database is mislukt met foutmelding:
 Database terugzetten mislukt! Database succesvol teruggezet! Database: Datum selecteren Datum-tijd selecteren Datum/datum-tijd sneltoetsen Standaardtaal: Verwijderen Exportgegevens verwijderen Verwijder een database Geselecteerd item verwijderen Verwijder de gekozen database. Verwijder database... E-Mail report Wijzig actie bestanden Voorkeuren gebruiker aanpassen Geselecteerd item aanpassen Versies widgets E-mail E-mail instellingen  Codering: Fout Database maken mislukt! Fout bij openen CSV-bestand Fout bij het importeren van deze gegevens:
%s
Foutmelding:
%s

%s Fout:  Uitzondering: Exporteer naar CSV Niet waar Veldnaam Bestand "%s" niet gevonden Actie bestanden Bestandstype Acties bestanden... Bestand om te importeren: Terug te zetten bestand: Vind Ga naar item-ID Ga naar item-ID: Hoogte: Host / Database information Uur: ID: Afbeeldingsgrootte Afbeelding te groot! Afbeeldingen Importeer uit CSV Fout bij importeren! Ongeldige invoer in formulier! Een met een wachtwoord beveiligde database kan niet verwijderd worden.
Voer deze handeling handmatig uit. Een met een wachtwoord beveiligde database kan niet terug gezet worden.
Voer deze handeling handmatig uit. Sneltoetsen Laatste wijzigingsdatum: Laatste wijziging door: Launch action Links Legenda van beschikbare sjablonen:  Limiet:  Limiet: Regels overslaan: Koppeling Inlognaam M Manage Shortcut Mark line for deletion Minuut: Wachtwoord admin ontbreekt! Model: N Naam Netwerkfout. New Naam nieuwe database: Maak een nieuwe database: Volgende Volgende item Volgend element Nee Lege opdracht Geen handeling gedefinieerd! Geen beschikbare plugin voor deze hulpbron! Geen verbinding met server Geen database aanwezig; maak een nieuwe aan! Geen andere taal beschikbaar! Geen item geselecteerd! Geen gegevens om af te drukken! Open Open reservekopie om te herstellen... Item openen Open de kalender Openen... Zoek / open relatie Handeling mislukt!
Foutmelding:
%s Handeling in uitvoering PNG afbeelding (*.png) Wachtwoord: Wachtwoorden komen niet overeen! Plak geselecteerde tekst Een moment a.u.b.
Deze handeling kost even tijd... Poort: Voorkeur Voorkeuren Vorige Vorige item Vorig element Afdrukken Procedure afdrukken Procedure afdrukken (uitgebreid) Print report Profile Profile Editor Profile: Item opgeslagen! Items niet verwijderd! Items verwijderd! Relatie sneltoetsen Herladen Verwijderen Programmafout melden Rapporten Aanvragen (%s/%s) Herstellen Database terug zetten Herstel database uit bestand. Rechts SSL verbinding Opslaan Opslaan als Opslaan als... Exportgegevens opslaan Save Tree Expanded State Bewaar breedte/hoogte Bewaar dit item Zoeken Zoek / open een item Zoek limiet instellen Zoek limiet... Item zoeken Beveiligingsrisico! Selecteer je handeling. Selectie Verbinding met server: Server instelling: Server verbinding: Server: Verbinden met server... Show plain text Sorry, verkeerde wachtwoord voor de Tryton server. Probeer het opnieuw.  Spellingscontrole uitvoeren Statusbalk Onderwerp: Omschakelen Wijzig beeld Positie tabbladeren Tekstscheidingsteken: Sneltoetsen in tekstvakken De naam van de database is beperkt tot alfa-numerieke karakters en "_" (underscore). Vermijd alle accenten, spaties en andere bijzondere karakters. The following action requires to close all tabs.
Do you want to continue? Het nieuwe wachtwoord voor admin komt niet overeen met de bevestiging.
 Dezelfde programmafout was al gemeld door een andere gebruiker.
Om je op de hoogte te houden is je gebuikersnaam toegevoegd aan de lijst van belangstellenden De identificatiecode van de server is gewijzigd sinds de laatste verbinding!
Het programma maakt geen verbinding meer met de server totdat de identificatiecode is hersteld. Deze software versie kan niet gebruik worden in combinatie met de versie op de server!  Deze database bestaat al! Dit is het wachtwoord van de Tryton server. Het is niet het wachtwoord van een gebruiker. Dit wachtwoord is doorgaans gedefinieerd in de trytond configuratie. Item is aangepast.
Wil je de wijzigingen opslaan? Tips Aan: Too much arguments Bovenkant Vertaal aanzicht Waar Tryton verbinding Wachtwoord Tryton server: Admin wachtwoord opnieuw invoeren Kan omgeving %s niet instellen Kan configuratiebestand %s niet opslaan!  Onbekend Unmark line for deletion Database bijwerken: Gebruik "%s" als sjabloon voor de bestandsnaam. Gebruikersnaam: Gebruiker: Bekijk logregister Openstaande verzoeken: %s ontvangen - %s verzonden Wat is de naam voor deze export? Breedte: Assistent Je werkt nu met het gekopieerde item! Overschrijven Verkeerde wachtwoord voor de Tryton server
Probeer het opnieuw. Verkeerde wachtwoord voor de Tryton server
Probeer het opnieuw. Ongeldige tekens in database naam! Verkeerde pictogram voor de knop!  J Ja U gaat een Tryton database verwijderen.
Weet u zeker dat u door wil gaan? U heeft geen toegang tot het systeem!
Controleer of er een menu is gedefinieerd voor deze gebruiker. Gebruik:
* + om de datum te verhogen
* - om de datum te verlagen of te wissen
* = om de huidige datum te kiezen

Beschikbare variabelen zijn:
h (hours)voor uren
d (days) voor dagen
w (weeks) voor weken (alleen met +/-)
m (month) voor maanden
y (years) voor jaren

Voorbeeld:
"+21d" verhoogt de datum met 21 dagen
"=11m" zet de datum op maand 11 van het jaar
"-2w" zet de datum met 2 weken terug Je moet één item selecteren! Eerst een regel selecteren voordat deze optie gebruikt kan worden! U moet eerst een import bestand selecteren! Gegevens eerst opslaan voordat u een vertaling toevoegt!  Uw selectie: I_nfo... _Acties.. _Toevoegen Reservekopie van database...  _Annuleren Tabblad sluiten _Verbinden... _Standaard Verwijderen...  Verbinding verbreken Volgende keer een tip _Dupliceren E-mail Installeer een plugin _Exporteer gegevens...  _Bestand Formulier Ga naar item ID... _Help _Thuis Pictogrammen... _Importeer gegevens...  Sneltoetsen _Manage profiles _Menu Toggle _Menubalk _Modus _Nieuw _Nieuwe database... _Volgende _Normaal _Opties _PDA _Plugins _Voorkeuren... V_orige Afdrukken Afsluiten Lees mijn aanvragen He_rladen _Ongedaan maken _Verwijderen _Herstel database... Op_slaan _Sla opties op Verzend een verzoek _Snelkoppelingen Wijzig beeld _Tekst _Tekst en pictogrammen _Tips... _Werkbalk _Gebruiker  d u logboek bijhouden op INFO niveau m specificeer een vervangend configuratiebestand specificeer kanalen voor het logboek specificeer het logboek niveau: INFO, DEBUG, WARNING, ERROR, CRITICAL specificeer de inlog gebruiker Stel de servernaam in  Stel de serverpoort in w y 