��    ^                   �     �               '     <     N     V     X  �  j  R     x   b     �     �               4  H   P     �     �     �     �     �     �     �  F   �     C  	   S     ]     y  #   �  %   �     �     �     �     �               $     A     G     N     Z     ^     m     u     ~  �   �  &   V     }  a   �     �  #     #   '  �   K  �     �   �  �   _      !  	   !     !     &!     .!     D!     \!     i!  +   �!  2   �!     �!     �!     "     &"     :"     N"     g"     �"     �"     �"  	   �"     �"  )   �"     #     #  )   9#     c#     y#     �#  ,   �#     �#     �#  	   $     $     '$     ;$     [$     m$     t$     �$     �$     �$     �$     �$     �$     �$     %     &%     6%     <%  	   S%     ]%     c%     |%  <   �%     �%  
   �%     �%     �%  
   �%     &     &  	   #&     -&     >&     N&     _&     d&  	   m&     w&     &     �&     �&  
   �&     �&     �&     �&     �&     �&  i   �&  l   h'     �'     �'     (     (     ((  !   -(     O(     ^(     c(     i(     k(     {(     �(     �(     �(     �(     �(     �(     �(     �(     �(     �(     �(     )     )     )     *)  &   =)     d)  '   s)     �)     �)     �)     �)     �)     *     *     "*     **  #   ?*     c*     y*  	   �*     �*     �*  /   �*     �*  
   �*     +     +     +     &+     6+     <+     K+     d+     q+     y+     �+     �+     �+     �+     �+     �+     �+  
   �+     �+     ,     ,     ,     +,     K,     Q,     `,  
   h,     s,     ,     �,     �,     �,     �,     �,     �,     �,     -     -  	   1-     ;-     N-     \-     o-     w-     �-  >   �-     �-  	   �-     �-     .     .     .     (.     8.  I   O.  =   �.  }   �.  �   U/  6   �/  !   0  �   <0  6   �0     �0     1     1     1     1     .1     31     E1     ]1     {1     �1     �1     �1  +   �1  
    2     2     2  '   2      G2     h2     o2  (   v2     �2  .   �2  /   �2  "   3     .3     I3     K3  J   O3  P   �3  k  �3     W5  -   v5  %   �5  7   �5     6  	   6     6     (6     -6     A6  
   I6     T6     `6  
   i6     t6     �6  
   �6  	   �6     �6     �6     �6     �6     �6     �6     �6      7     7     7     .7     ?7     L7     U7     [7     `7     q7     w7     7     �7     �7     �7  	   �7  	   �7     �7     �7     �7     �7     �7     �7     8     8     8  
   +8     68     C8     I8     Y8     b8     h8     j8      l8     �8     �8     �8  <   �8     9     9     59     M9     O9  �  Q9     ;     ;     +;     ?;     V;  	   m;     w;     y;  �  �;  R   %>  �   x>     ?     &?     =?     N?     n?  D   �?     �?     �?     �?     �?     �?     @     @  O   9@     �@     �@  +   �@     �@  '   �@  '   A     6A     DA  
   QA     \A  
   qA     |A     �A     �A     �A     �A     �A     �A     �A  
   �A  
   �A  �   �A  "   �B     �B  a   C     tC  +   �C  '   �C  �   �C  �   �D  �   -E  �   �E     �F     �F     �F     �F     �F     �F  
   �F     �F  >   G  -   SG     �G     �G     �G     �G     �G      H     H     6H     IH     aH  	   wH  %   �H  4   �H     �H  "   �H  /   I     OI     jI     �I  1   �I     �I     �I  
   J     J     "J     7J     TJ     eJ     lJ     {J     �J     �J     �J     �J     �J      �J     K     3K     DK  "   JK     mK     zK  "   �K  #   �K  ?   �K     L  	   L     L     2L     ;L     GL     [L     lL     xL     �L     �L     �L     �L     �L     �L     �L     �L     M     	M  (   M     DM     MM     `M     tM  e   �M  b   �M     SN     gN     �N     �N     �N     �N     �N     �N     �N     �N     �N     O     O     %O     CO     JO     LO     QO     _O     cO     {O     �O     �O     �O     �O     �O      �O  9   P     @P  :   PP     �P     �P     �P  	   �P  *   �P     Q     $Q     9Q     FQ  %   aQ     �Q     �Q     �Q     �Q     �Q  :   �Q     R  
   R  
   *R     5R     AR     UR     gR     lR  %   �R     �R     �R     �R     �R     �R     �R     S     S     *S  	   2S     <S     LS     TS     eS     mS     S     �S     �S     �S     �S     �S     �S     �S     	T     T     &T     CT     YT     iT     xT     �T     �T     �T     �T     �T     �T      �T     U  >   U     SU  	   fU     pU  	   |U     �U     �U     �U     �U  I   �U  3   V  s   KV  �   �V  .   MW  )   |W  �   �W  ,   :X     gX     lX     rX     �X     �X     �X     �X     �X     �X     �X  &   
Y  	   1Y     ;Y  0   TY     �Y  
   �Y     �Y  '   �Y      �Y  	   Z  	   Z  )   Z     BZ  .   QZ  .   �Z  "   �Z     �Z     �Z     �Z  ;   �Z  u   3[  X  �[  %   ]  /   (]  +   X]  8   �]     �]     �]     �]     �]     �]     ^     ^     ^  
   -^     8^     F^     O^     o^  	   {^     �^     �^     �^  
   �^     �^     �^     �^     �^     �^     _     _     0_     =_     N_     V_     ]_     p_     �_     �_     �_     �_     �_     �_     �_     �_     �_     �_     �_  
   `     `     (`     1`     D`     W`     _`     p`     v`     �`  
   �`     �`     �`      �`     �`  +   �`     �`  A   
a     La  #   ia  )   �a     �a     �a    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Create a new record Create new database Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Lines to Skip: Link Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Unknown Unmark line for deletion Use "%s" as a placeholder for the file name User name: User: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version:  1.0
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2008-12-24 18:53+0100
PO-Revision-Date: 2011-10-24 01:03+0200
Last-Translator: Dan Horák <dan@danny.cz>
Language-Team: Czech
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  z  %d záznam načten! %d záznam uložen! %d záznamů načteno! %d záznamů uloženo! soubor %s : <b>Všechna pole</b> <b>Znáte Tryton, jednoho ze jmenovců našeho projektu?</b>

Triton (vysloveno /ˈtraɪtən/ TRYE-tən, nebo jako v řečtině Τρίτων) je
největší měsíc planety Neptun, objevený 10. října 1846
Williamem Lassellem. Je to jediný velký měsíc ve sluneční soustavě
s retrográdním oběhem, což je oběh s opačnou rotací, než je rotace
planety. S průměrem 2700 km jde o sedmý největší měsíc sluneční
soustavy. Triton zahrnuje 99.5 procenta známé hmoty obíhající Neptun,
včetně prstenců planety a dalších 12 známých měsíců. Je také hmotnější
než všech 159 menších měsíců ve sluneční soustavě dohromady.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Seznam exportovaných záznamů</b>
Ze seznamu můžete zkopírovat záznamy pomocí Ctrl + C
a vložit je do libovolné aplikace pomocí Ctrl + V
 <b>Pole pro export</b> <b>Pole pro import</b> <b>Možnosti</b> <b>Předdefinované exporty</b> <b>Vítejte v Trytonu</b>


 Databáze se stejným názvem již existuje.
Použijte jiný název. Přístup odepřen! Akce Akce Přidat Přidat překlad Přidat jména polí Přidat přílohu k záznamu Heslo správce a jeho zopakování jsou nutné pro vytvoření nové databáze. Heslo správce: Všechny soubory zadejte alternativní konfigurační soubor Chyba aplikace! Opravdu chcete odstranit tento záznam? Opravdu chcete odstranit tyto záznamy? Příloha(%d) Přílohy(0) Příloha: Automatická detekce Zálohovat Zálohovat databázi Zálohovat vybranou databázi. Tělo: Dole Sledování chyb CC: Parametry CSV Změnit _Připojit _Vytvořit Databázi nelze vytvořit, příčina je neznámá.
Pokud je databáze již vytvořená, může být poškozená. Zkuste ji odstranit. Zkontrolujte chybové hlášení pro případné další informace.
Chybová zpráva:
 Zrušit spojení k Tryton serveru. Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Vyberte zásuvný modul Vyberte Tryton databázi pro zálohování: Vyberte Tryton databázi pro smazání: Vyberte heslo pro správce nové databáze. Pomocí následujících údajů se pak budete schopni k této databázi připojit:
Uživatel: admin
Heslo: <právě zadané heslo> Vyberte výchozí jazyk, který bude nainstalován pro tuto databázi. Po instalaci budete schopni instalovat další jazyky pomocí administračního menu. Vyberte název databáze pro obnovu.
Povolené znaky jsou alfanumerické a _ (podtržítko)
Nepoužívejte znaky s akcenty, mezeru nebo speciální znaky!
Příklad: tryton Vyberte název nové databáze.
Povolené znaky jsou alfanumerické a _ (podtržítko)
Nepoužívejte znaky s akcenty, mezeru nebo speciální znaky!
Příklad: tryton Vymazat Zavřít záložku Příkazový řádek: Porovnat Výjimka při souběhu Potvrzení hesla správce: Potvrzení Připojit k Tryton serveru Chyba při spojení!
Špatné uživatelské jméno nebo heslo! Chyba spojení!
Nelze se připojit k serveru! Kopírovat vybraný text Nelze se spojit se serverem! Vytvořit nový záznam Vytvořit novou databázi Vytvořit novou relaci Vytvořit novou databázi. Založena nová chyba s ID Datum vytvoření: Vytvořeno uživatelem: Vyjmout vybraný text Databáze Databáze úspěšně zazálohována! Odstranění databáze selhalo s chybovou zprávou:
 Odstranění databáze selhalo! Databáze úspěšné odstraněna! Záloha databáze selhala s chybovou zprávou:
 Záloha databáze selhala! Databáze je chráněna heslem! Název databáze: Obnovení databáze selhalo s chybovou zprávou:
 Obnova databáze selhala! Databáze úspěšně obnovena! Databáze: Výběr datumu Výběr data a času Zkratky pro data/data a čas Výchozí jazyk: Smazat Smazat exportu Smazat databázi Smazat vybraný záznam Smazat vybranou databázi. Smazat databázi E-Mail report Upravovat akce pro soubory Upravit uživatelské nastavení Upravit vybraný záznam Editovací prvky Email Poslat nastavení programu emailem Kódování: Chyba Chyba při vytváření databáze! Chyba při otevírání CSV souboru Chyba při importu tohoto záznamu:
%s
Chybová zpráva:
%s

%s Chyba: Výjimka: Exportovat do CSV souboru Nepravda Jméno pole File "%s" not found Akce pro soubory Typ souboru Akce pro soubory... Soubor k importu: Obnovit ze souboru: Najít Přejít na ID Přejít na ID: Výška: Host / Database information Hodina: ID: Velikost obrázku Rozměry obrázku jsou příliš velké! Obrázky Import CSV souboru Chyba načítání! Neplatný formulář! Není možné zálohovat databázi chráněnou heslem.
Záloha a obnova musí být provedeny ručně. Není možné obnovit databázi chráněnou heslem.
Záloha a obnova musí být provedeny ručně. Klávesové zkratky Datum poslední úpravy: Poslední úpravu provedl: Launch action Vlevo Seznam dostupných náhrad: Vynechané řádky: Odkaz Přihlášení M Manage Shortcut Mark line for deletion Minuta: Chybí heslo administrátora! Model: N Name Chyba sítě! New Název nové databáze: Nastavení nové databáze: Další Následující záznam Další prvek Ne Žádný řetězcový atribut Žádná akce není definována! Pro tento zdroj není dostupný žádný zásuvný modul! Není spojení! Nenalezena žádná databáze, musíte nějakou vytvořit! Jiný jazyk není k dispozici! Žádný záznam není vybrán! Není co tisknout! Otevřít Otevřít soubor se zálohou pro obnovu... Otevřít záznam Otevřít kalendář Otevřít... Otevřít/prohledat relaci Operace selhala.
Chybová zpráva:
%s Operace probíhá Obrázek PNG (*.png) Heslo: Hesla nesouhlasí! Vložit vybraný text Prosím čekejte,
tato operace bude nějakou dobu trvat... Port: Nastavení Nastavení Předchozí Předchozí záznam Předchozí prvek Tisk Tisknout průběh práce Tisknout průběh práce (komplexní) Print report Profile Profile Editor Profile: Záznam uložen! Záznamy neodstraněny! Záznamy odstraněny! Zkratky pro relace Obnovit Odstranit Nahlásit chybu Sestavy Requests (%s/%s) Obnovit Obnovit databázi Obnovit databázi ze souboru. Vpravo SSL spojení Uložit jako Uložit jako... Uložit exportu Save Tree Expanded State Save Width/Height Uložit tento záznam Hledat Vyhledat / Otevřít záznam Search Limit Settings Search Limit... Hledat záznam Security risk! Vyberte akci Výběr Přepojení k serveru: Nastavení serveru: Spojení k serveru: Server: Nastavení spojení k serveru... Show plain text Zadali jste špatné heslo pro Tryton server. Zkuste to znovu. Kontrola pravopisu Statusbar Předmět:  Přepnout Přepnout pohled Pozice záložek Oddělovač textu: Zkratky pro texty The following action requires to close all tabs.
Do you want to continue? Heslo správce a jeho zopakování nejsou shodné.
 Ta samá chyba již byla nahlášena jiným uživatelem.
Vaše uživatelské jméno bylo přidáno na její seznam. The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. Verze klienta není kompatibilní se serverem! Databáze s tímto názvem již existuje! Toto je heslo pro Tryton server. Nepatří žádnému skutečnému uživateli. Toto heslo je obvykle definováno v konfiguračním souboru serveru. Tento záznam byl upraven
chcete ho uložit? Tipy Komu: Too much arguments Nahoře Přeložit pohled Pravda Tryton spojení Heslo pro Tryton server: Zadejte znovu heslo správce Nemohu nastavit lokalizaci %s Nelze zapsat konfigurační soubor %s! Neznámý Unmark line for deletion Použijte "%s" jako náhradu pro jméno souboru. Uživatelské jméno: Uživatel: Zobrazit záznamy... Waiting requests: %s received - %s sent What is the name of this export? Šířka: Průvodce Nyní pracujete s duplicitním záznamem! Přesto zapsat Špatné heslo Tryton serveru
Zkuste to znovu. Špatné heslo Tryton serveru
Zkuste to znovu. Neplatný znak v názvu databáze! Špatná ikona pro tlačítko! Y Ano Bude smazána Tryton databáze.
Opravdu chcete pokračovat? Nemůžete se přihlásit k tomuto systému!
Zkontrolujte, jestli má váš uživatel nadefinovanou hlavní nabídku. Můžete použít speciální operátory:
* + pro zvýšení data
* - pro snížení data
* = pro nastavení data nebo pro aktuální den

Dostupné proměnné:
h pro hodiny
d pro dny
w pro týdny
m pro měsíce
y pro roky

Příklady:
"+21d" přidání 21 dní k datu
"=11m" nastavení data na 11. měsíc roku
"-2w" snížení data o 2 týdny Musíte vybrat právě jeden záznam! Musíte vybrat záznam před použitím relace! Nejprve je nutné vybrat soubor pro import! Musíte uložit záznam před přidáváním překladů! Váš výběr: _O programu... Akce... _Přidat Zazálohovat databázi... _Zrušit _Zavřít kartu _Připojit... _Výchozí _Odstranit... _Odpojit Zobrazit příště další tip _Duplikovat _Email... Spustit zásuvný modul Exportovat data _Soubor Formulář Přejít na záznam ID... Nápo_věda _Domů _Ikony Importovat data... _Klávesové zkratky... _Manage profiles _Menu Toggle Hlavní nabídka _Režim _Nový Nová databáze... _Následující _Normální _Volby _PDA Zásuvné moduly _Vlastnosti... _Předchozí _Tisk... Konec... Přečíst mé požadavky _Obnovit Obnovit/Zpět _Odstranit Obnovit databázi... _Uložit Uložit nastavení Odeslat požadavek Zkratky Přepnout pohled _Text _Text a ikony _Tipy... _Uživatel d h logging everything at INFO level m zadejte alternativní konfigurační soubor zadejte kanály pro logování určete logovací úroveň: INFO, DEBUG, WARNING, ERROR, CRITICAL zadejte uživatelské jméno zadejte název hostitele pro server zadejte číslo portu síťového serveru w y 