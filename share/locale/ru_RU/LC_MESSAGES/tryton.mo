��    d     <              \     ]     b     v     �     �     �     �     �  �  �  R   o  x   �     ;     S     k     z     �  H   �     �                         +     <  F   \     �  	   �     �     �  #   �  %        6     E     S     _     k     r     �     �     �     �     �     �     �     �     �  �   �  &   �     �  a   �     S  #   c  #   �  �   �  �   f  �   
   �   �      h!  	   n!     x!     �!     �!     �!     �!     �!  +   �!  2   "     B"     U"     r"     �"     �"     �"     �"     �"     �"     �"     #  	   "#     ,#  )   L#     v#     �#  )   �#     �#     �#     $  ,   $     G$     `$  	   �$     �$     �$     �$     �$     �$     �$     �$     %     %     :%     L%     Z%     m%     �%     �%     �%     �%  	   �%     �%     �%     �%  <   &     B&  
   J&     U&     c&  
   i&     t&     �&  	   �&     �&     �&     �&     �&     �&  	   �&     �&     �&     '     '  
   '     "'     8'     ?'     O'     b'  i   p'  l   �'     G(     Z(     t(     �(     �(  !   �(     �(     �(     �(     �(     �(     �(     �(     )     )     +)     2)     4)     9)     H)     L)     _)     s)     x)     �)     �)     �)     �)  &   �)     �)  '   �)     *     1*     E*     W*     \*     {*     �*     �*     �*  #   �*     �*     �*  	   +     +     '+  /   9+     i+  
   o+     z+     �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     
,     ,     -,     >,     Y,     `,  
   g,     r,     z,     �,     �,     �,     �,     �,     �,     �,  
   �,     �,     �,     -     (-     9-     @-     W-     m-     }-     �-     �-  	   �-     �-     �-     �-     �-     �-     .  >   $.     c.  	   r.     |.     �.     �.     �.     �.     �.  �   �.  I   ]/  =   �/  }   �/  �   c0  6   �0  !   (1  �   J1  6   �1     2     2     2     )2     -2     <2     A2     S2     k2     �2     �2     �2     �2     �2  +   �2  
   3     *3     03  '   >3      f3     �3     �3  (   �3     �3  .   �3  /   �3  "   *4     M4     h4     j4  J   n4  P   �4  k  
5     v6  -   �6  %   �6  7   �6     !7  	   17     ;7     G7     L7     `7  
   h7     s7     7  
   �7     �7     �7  
   �7  	   �7     �7     �7     �7     �7     �7     8     8     8     &8     68     M8     ^8     k8     t8     z8     8     �8     �8     �8     �8     �8     �8  	   �8  	   �8     �8     �8     �8     �8     	9     9     &9     ,9     :9  
   J9     U9     b9     h9     x9     �9     �9     �9     �9      �9     �9     �9     �9  <   �9     *:     A:     ]:     u:     w:  �  y:     c<  +   h<  #   �<  -   �<  %   �<     =     =     =  �  3=  R   !B  �   tB  '   \C  %   �C     �C  8   �C  ,   �C  �   "D     �D     �D     �D     �D     E      -E  >   NE  �   �E  .   'F     VF  J   fF  "   �F  8   �F  ?   G     MG     cG     xG     �G  )   �G  ?   �G  ?   H     TH  
   hH  5   sH     �H     �H     �H     �H     I  �  I  5   �J  #   �J  �   �J  %   �K  a   �K  K   XL  t  �L    N  Z  5O  Z  �P     �Q     �Q      R     ;R  <   LR  A   �R     �R  (   �R  h   S  c   tS  6   �S  ?   T  &   OT  /   vT  $   �T  C   �T  V   U     fU  )   �U     �U  1   �U     �U  8   V  e   HV  9   �V  '   �V  �   W  c   �W  6   �W     .X  Y   KX  C   �X  D   �X     .Y     DY  %   XY  @   ~Y  !   �Y     �Y  2   �Y  $   #Z  ,   HZ  7   uZ     �Z     �Z  6   �Z  <   [  0   >[     o[     �[  G   �[     �[     �[  4   �[  ,   2\  �   _\     �\     �\     ]     ]  !   *]     L]     l]     �]  '   �]     �]  -   �]  
   ^      ^  7   :^     r^     �^     �^     �^  #   �^  B   �^     (_      ?_  #   `_  $   �_  �   �_  �   �`  ?   �a     �a  ,   �a     b  
   b  2   *b     ]b  %   xb     �b     �b     �b     �b     �b     �b  E   �b     >c     Lc     Nc     Sc     pc  2   tc  4   �c     �c  !   �c     d     +d  ,   2d  +   _d  P   �d     �d  `   �d  B   Ze  )   �e     �e     �e  L   �e     Cf  "   _f     �f  (   �f  a   �f  '   g  "   Gg     jg  %   xg  6   �g  {   �g     Qh     Zh     mh     �h  !   �h     �h     �h     �h  G   �h     Fi     Si     [i     ji      si  !   �i     �i  ;   �i     j      j     /j     Lj     Yj     rj  .   �j  ?   �j     �j     k     !k     4k     Nk  6   kk     �k  !   �k  &   �k  
   l  (   l     8l     Nl     ^l     ll  !   {l  
   �l  "   �l  "   �l  "   �l     m  ;   m     [m  �   km  %   �m     n  	   2n     <n     Zn  !   xn  $   �n  >   �n    �n  I   
p  v   Tp    �p  �   �q  Q   ur  @   �r  �   s  Z   �s     Ot  	   \t     ft     yt  !   �t     �t     �t  #   �t  M   �t  B   Gu  K   �u     �u     �u  '   v  c   ,v     �v     �v  #   �v  S   �v      Aw     bw     ow  Q   |w     �w  �   �w  �   jx  L   �x  D   By     �y     �y  �   �y  �   z  8  �z  7   �|  f   }  O   r}  h   �}     +~     >~     T~     e~     w~     �~     �~     �~     �~     �~     �~  T        `     u  (   �     �     �  
   �  $   �     �     �     !�     .�     H�     e�     v�  	   ��  
   ��  
   ��  #   ��     ǀ     ڀ     �     �     �     -�     C�     X�     e�  %   t�     ��  !   ��     ΁     ށ     ��  %   
�  !   0�     R�     _�  
   �     ��     ��  %   ��     ܂     ��     ��  :   ��     7�  U   :�  7   ��  h   ȃ  .   1�  $   `�  &   ��     ��     ��    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Create a new record Create new database Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit: Lines to Skip: Link Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version: tryton 1.3.0
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2009-10-17 11:11+0200
PO-Revision-Date: 2011-10-24 01:07+0200
Last-Translator: Dmitry Klimanov <k-dmitry2@narod.ru>
Language-Team: ru_RU <k-dmitry2@narod.ru>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
 из %d запись импортирована! %d запись сохранена! %d записей импортировано! %d записей сохранено! %s файл:  : <b>Все поля</b> <b>Знаете ли вы, Тритон, один из однофамильцев нашего проекта?</b>

Тритон (произносится / traɪtən / TRYE-tən, или, как в греческом Τρίτων) является
крупным спутником планеты Нептун, его обнаружил 10 октября 1846 года
 Уильям Лассел. Это единственный большой спутник в Солнечной системе
с ретроградной орбитой, орбита которая направлена обратно
вращению планеты. 2700 км в диаметре, это седьмое крупнейшие
спутник в Солнечной системе. Тритона составляет более 99,5 процента всех
Средства массовой известны орбиты Нептуна, в том числе кольцами планеты и двенадцатью
другими известными спутниками. Кроме того, более массивный, чем все спутники Солнечной системы
159 известных меньших спутников, вместе взятых.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Экспортировать список записей</b>

Вы можете копировать записи из любого списка по Ctrl + C
и вставить в любое приложение по Ctrl + V
 <b>Поля для экспорта</b> <b>Поля для импорта</b> <b>Опции</b> <b>Предопределенные экспорты</b> Добро пожаловать в Tryton


 База данных с таким же названием уже существует.
Попробуйте другое имя базы данных. Доступ запрещен! Действие Действия Добавить Добавить перевод Добавить имя поля Добавить вложение для этой записи Пароль администратора и подтверждения, необходимые для создания новой базы данных. Административный пароль: Все фалы Всегда игнорировать это предупреждение. Ошибка приложения! Вы уверены, удалить эту запись? Вы уверены, что удалить эти записи? Вложений (%d) Вложений (0) Вложение: Авто-обнаружение Резервное копирование Резервное копирование базы данных Резервное копирование базы данных Сообщение: Снизу Сопроводитель ошибок (Bug Tracker) Скрытая копия: Параметры CVS Изменить Соединение С_оздать Невозможно создать базу данных, вызванное неизвестной причине.
Если база данных создана, она может быть неполной. Удалите эту База данных! Проверьте сообщение об ошибке для возможной информации.
Сообщение об ошибке:
 Отмена подключения к серверу Сменить ускоритель Проверить наличие автоматического обновления базы данных после восстановления базы данных из предыдущей версии Tryton Выберите дополнение Выберите базу данных для резервного копирования Tryton: Выберите базу данных, чтобы для удаления: Выберите пароль для администрирования пользователей новой базы данных.Обладая этим полномочием вы будете иметь возможность войти в базу данных:
Имя пользователя: Admin
Пароль: <пароль вы установите здесь> Выберите язык по умолчанию, который будет установлен для этой базы данных. Вы сможете установить новые языки после установки через административное меню. Выберите имя базы данных, для восстановления.
Разрешено символьно-цифровых символов или подчеркивания (_)
Необходимо, чтобы избежать все акценты, пробел или специальные символы!
Пример: Tryton Выберите имя базы данных, для восстановления.
Разрешено символьно-цифровых символов или подчеркивания (_)
Необходимо, чтобы избежать все акценты, пробел или специальные символы!
Пример: Tryton Очистить Закрыть вкладку Командная строка: Сравнить Прерывание конкурентных записей Подтвердите пароль администратора: Подтвердите Соединение с сервером Ошибка соединения
Не верный имя пользователя или пароль! Ошибка соединения!
Невозможно соединиться с сервером! Скопировать выделенный текст Не удается подключиться к серверу! Создать новую запись Создать новую базу данных Создать новую связь Создать новую базу данных по шаблону Создан новый отчет об ошибке с идентификатором Дата создания Создано пользователем Текущий пароль: Вырезать  выделенный текст База данных База данных сохранена успешно! Удаление базы данных завершено с сообщением об ошибки:
 Не удалось удалить Базу данных! База успешно удалена! Резервное копирование базы данных завершилось с сообщением об ошибки:
 Не удалось сделать Резервное копирование базы данных! База данных защищена паролем! Имя базы данных Восстановление базы данных с ошибкой сообщение:
 База данных восстановить не удалось! База данных восстановлена полностью! База данных Выбор даты Выбор даты и времени Дата/Время записи быстрого доступа Язык по умолчанию: Удалить Удалить настройку экспорта Удалить базу данных Удалить выбраную запись Удалить выбранную базу данных Удалить E-Mail report Редактировать файлы Действия Изменить настройки пользователя Изменить выбранную запись Версия Widgets Эл.почта Настройки программы электронной почты Кодировка: Ошибка Ошибка создания базы данных! Ошибка открытия файла CSV Ошибка при попытке импортировать эту запись:
%s
Сообщение об ошибке:
%s

%s Ошибка:  Прерывание: Экспорт в CSV Ложный Наименование поля Файл "%s" не найден Файл действий Тип файла Действия над файлом... Файл для импорта: Файл для восстановления: Найти Перейти на строку Введите идентификатор строки: Ввысота Host / Database information Часов: Номер строки Размер изображения Размер изображения слишком большой! Изображения Импорт из файла CSV Ошибка при импорте! Недопустимая форма! Не возможно сделать dump на защищенной паролем базе данных. 
Резервное копирование и восстановление необходимо выполнить. Вручную Не возможно восстановить на защищенной паролем базе данных.
Резервное копирование и восстановление необходимо выполнить. Вручную Сочетания клавиш быстрого доступа Дата изменения: Изменена пользователем: Launch action Влево Легенда о наличии Маркеров: Кол-во записей Строки для пропуска: Ссылка Регистрация М Manage Shortcut Mark line for deletion Минут: Отсутствует административный пароль! Модель: N Name Сетевая ошибка! New Название новой базы данных: Установка новой базы данных: Следующий Следующая запитсь Следующий Widget Нет Нет строковых атрибутов Не определены действия! Нет доступных дополнений для этого ресурса! Нет соединения! База данный не найдена, вы должны сначала создать её! Отсутствуют другие доступные языки! Нет выбранных записей! Не чего печатать! Открыть Запустите программу для восстановления... Открыть запись Открыть календарь  Открыть... Открыть / Поиск связей Не удалось выполнить операцию!
Сообщение об ошибке:
%s Операция исполняется PNG изображение (*.png) Пароль: Пароли не совпадают! Вставить скопированный текст Пожалуйста, подождите,
Эта операция может занять некоторое время ... Порт Настройка Настройки Предыдущий Предыдущая запись Предыдущий Widget Печать Печать процесса Распечатать бизнес процесс (полностью) Print report Profile Profile Editor Profile: Запись сохранена! Записи не удалены! Записи удалены! Связи записей "быстрого доступа" Обновить Удалить Отчет об ошибке Отчеты Запросов (%s/%s) Восстановить Восстановить базу данных Восстановить базу данных из файла. В право SSL соединение Сохранить Сохранить как Сохранить как... Сохранить настройку экспорта Save Tree Expanded State Сохранить размеры Сохранить эту запись Поиск Поиск / Открыть запись Search Limit Settings Search Limit... Поиск... Security risk! Выберите действие Выбор Сервер соединения: Настройка сервера: Сервер соединения: Сервер: Настройка соединения с сервером Show plain text Извините, неверный пароль для сервера. Пожалуйста, попробуйте еще раз. Проверка орфографии Строка состояния Тема: Переключить вид Переключить вид Положение вкладок Разделитель текста: Горячие клавиши в текстовых полях Имя базы данных ограничивается символами и цифрами и " _ " (Подчеркивание). Избегайте все акценты, пространство и любые другие специальные символы.  The following action requires to close all tabs.
Do you want to continue? Новый пароль администратора не совпадает с полем подтверждения
 Эта же ошибка была уже сообщена от другого пользователя.
Чтобы держать вас в курсе ваше имя пользователя будет добавлен в список перечень по этому вопросу The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. Эта версия клиента не совместима с сервером! Это имя базы данных уже существует! Это пароль для сервера Tryton. Он не относится к реальному пользователю. Этот пароль, обычно определяется в серверной конфигурации. Эта запись была изменена
Вы хотите его сохранить? Советы Кому: Too much arguments Вверху Просмотр перевода Истинный Tryton соединение Пароль сервера Tryton: Введите административный пароль повторно Не удается установить локализацию %s Невозможно записать файл конфигурации %s! Неизвестно Unmark line for deletion Обновить базу данных: Используйте "%s" в качестве заполнителя для имен файлов Имя пользователя Пользователь Просмотр изменений Ожидающие запросы - Получено: %s , Отправлено: %s What is the name of this export? Ширина Мастер Сейчас работает на дублирующихся записи(ей)! Записать Неверный административный пароль сервера Tryton
Пожалуйста, попробуйте снова Неверный административный пароль сервера Tryton
Пожалуйста, попробуйте снова Недопустимые символы в имени базы данных! Недопустимое изображение для кнопки! Г Да Вы собираетесь удалить базу данных.
Вы действительно уверены, продолжить? Вы не можете войти в систему!
Проверьте, если у вас меню на пользователя Вы можете использовать специальные операторы:
* + прибавления дня
* - уменьшения дня
* = установить дату или текущий день

Доступные переменные:
h для часов
d для дней
w для недель (только +/-)
m для месяцев
y для годов

Например:
"+21d" увеличить на 21 день
"=11m" установить дату на 11-й месяц в году
"-2w" сократить на 2 недели дату Вы должны выбрать одну запись! Необходимо выбрать запись для использования Отношения! Вы должны сначала выбрать файл для импорта! Необходимо сохранить запись перед добавлением перевода! Ваш выбор: О программе Действия _Добавить Сохранить Отмена Закрыть вкладку Подключение... По умолчанию Удалить Отключение Показывать  новый совет при следующем запуске Копировать _Эл.почта _Запустить дополнение Экспорт данных Файл Форма _Перейти на строку... Помощь В начала Значки Импорт данных _Быстрые доступ _Manage profiles _Menu Toggle _Меню Режим Новый Новая база данных... Следующая Нормальный _Параметры Упрощенный Дополнения Настройки... Предыдущая печать _Выход... Прочитать сообщения _Обновить Обновить/Отменить _Удалить Восстановить Сохранить Сохранить настройки Послать сообщение Ярлыки Сменить просмотр Текст Текст и Значки _Советы... Панель инструментов _Пользователь д ч регистрировать всё на уровне INFO м указать альтернативный конфигурационный файл определить каналы для журнала определить уровень ведения журнала: INFO, DEBUG, WARNING, ERROR, CRITICAL указать Имя пользователя укажите имя сервера укажите порт сервера н y 