��    d     <              \     ]     b     v     �     �     �     �     �  �  �  R   o  x   �     ;     S     k     z     �  H   �     �                         +     <  F   \     �  	   �     �     �  #   �  %        6     E     S     _     k     r     �     �     �     �     �     �     �     �     �  �   �  &   �     �  a   �     S  #   c  #   �  �   �  �   f  �   
   �   �      h!  	   n!     x!     �!     �!     �!     �!     �!  +   �!  2   "     B"     U"     r"     �"     �"     �"     �"     �"     �"     �"     #  	   "#     ,#  )   L#     v#     �#  )   �#     �#     �#     $  ,   $     G$     `$  	   �$     �$     �$     �$     �$     �$     �$     �$     %     %     :%     L%     Z%     m%     �%     �%     �%     �%  	   �%     �%     �%     �%  <   &     B&  
   J&     U&     c&  
   i&     t&     �&  	   �&     �&     �&     �&     �&     �&  	   �&     �&     �&     '     '  
   '     "'     8'     ?'     O'     b'  i   p'  l   �'     G(     Z(     t(     �(     �(  !   �(     �(     �(     �(     �(     �(     �(     �(     )     )     +)     2)     4)     9)     H)     L)     _)     s)     x)     �)     �)     �)     �)  &   �)     �)  '   �)     *     1*     E*     W*     \*     {*     �*     �*     �*  #   �*     �*     �*  	   +     +     '+  /   9+     i+  
   o+     z+     �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     
,     ,     -,     >,     Y,     `,  
   g,     r,     z,     �,     �,     �,     �,     �,     �,     �,  
   �,     �,     �,     -     (-     9-     @-     W-     m-     }-     �-     �-  	   �-     �-     �-     �-     �-     �-     .  >   $.     c.  	   r.     |.     �.     �.     �.     �.     �.  �   �.  I   ]/  =   �/  }   �/  �   c0  6   �0  !   (1  �   J1  6   �1     2     2     2     )2     -2     <2     A2     S2     k2     �2     �2     �2     �2     �2  +   �2  
   3     *3     03  '   >3      f3     �3     �3  (   �3     �3  .   �3  /   �3  "   *4     M4     h4     j4  J   n4  P   �4  k  
5     v6  -   �6  %   �6  7   �6     !7  	   17     ;7     G7     L7     `7  
   h7     s7     7  
   �7     �7     �7  
   �7  	   �7     �7     �7     �7     �7     �7     8     8     8     &8     68     M8     ^8     k8     t8     z8     8     �8     �8     �8     �8     �8     �8  	   �8  	   �8     �8     �8     �8     �8     	9     9     &9     ,9     :9  
   J9     U9     b9     h9     x9     �9     �9     �9     �9      �9     �9     �9     �9  <   �9     *:     A:     ]:     u:     w:  �  y:     <     <     -<     C<     `<  
   x<     �<     �<  �  �<  R   W?  �   �?     <@     U@     n@     ~@     �@  V   �@     A     !A     )A  	   2A     <A     RA     mA  e   �A     �A     B  !   B     @B  0   WB  2   �B     �B  
   �B     �B     �B     �B     �B      C     'C     /C     8C     OC     SC     cC  	   lC     vC  �   }C  %   jD     �D  �   �D     +E  +   ;E  )   gE  �   �E  �   ^F  �   G  �   �G     �H     �H     �H     �H     �H      �H     I     I  8   -I  5   fI     �I      �I     �I     �I     	J     J  #   7J     [J     oJ     ~J     �J     �J  >   �J  D   �J  &   9K  +   `K  8   �K  )   �K  2   �K     "L  :   ;L  -   vL  -   �L     �L     �L     �L     M     $M     ;M     BM     QM     jM      �M     �M     �M     �M     �M     N     .N     DN  1   XN     �N     �N      �N     �N  F   �N     "O     )O     5O     DO     JO      [O     |O     �O     �O     �O     �O  	   �O     �O  	   �O     P     P     'P     -P     1P     CP  	   bP     lP     P     �P  �   �P  ~   1Q     �Q     �Q     �Q     �Q  	   R     R     1R     :R     LR     SR     [R     ]R     mR     �R     �R     �R     �R     �R     �R     �R     �R  &   �R  	   S     S     0S     BS     ES     ]S  /   wS     �S  0   �S     �S      T     (T     =T  #   CT     gT     yT     �T     �T  (   �T     �T     �T      U     U      U  :   9U     tU     |U     �U     �U     �U     �U     �U     �U  %   �U     V     V     !V     0V     9V     NV     hV     V  	   �V     �V     �V     �V     �V  	   �V     �V  )   �V      W     (W     6W     =W     JW     ZW     jW     �W     �W     �W     �W     �W     �W     �W     X     X  
   ,X     7X     NX     kX  	   �X  *   �X     �X  K   �X     Y     0Y     @Y     HY     TY     bY     }Y     �Y  �   �Y  I   bZ  Q   �Z  �   �Z  �   �[  :   !\  (   \\  �   �\  3   ]     ?]     H]     K]     ^]     g]  	   v]     �]     �]  !   �]  (   �]  4   �]     .^     :^     S^  #   m^     �^     �^     �^  5   �^      �^     _  	   _  6   (_     __  Y   w_  >   �_  6   `  !   G`     i`     k`  ;   o`  T   �`  �   a     �b  5   �b  1   �b  :   c     Sc     bc     pc     }c  '   �c  	   �c     �c     �c     �c  
   �c     �c  %    d  	   &d     0d     Hd     \d     od     xd     �d     �d     �d     �d     �d     �d     �d     �d     �d     	e     e     e  
   .e     9e  	   Ae     Ke     Pe     Ye  	   je     te  	   �e     �e  	   �e     �e  	   �e     �e     �e     �e     �e     f     f     'f     .f     ?f     Lf     cf     lf     nf  )   pf     �f  0   �f  "   �f  M   �f     >g  "   Ug  "   xg     �g     �g    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Create a new record Create new database Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit: Lines to Skip: Link Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version: tryton 0.0.1
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2008-10-02 18:11+0500
PO-Revision-Date: 2011-10-24 01:04+0200
Last-Translator: Igor Támara <igor@tamarapatino.org>
Language-Team: es_ES <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  de  !%d registro importado! %d registro guardado! !Se importaron %d registros! %d registros guardados! archivo %s : <b>Todos los campos</b> <b>¿Conoce Triton? uno de los apelativos de nuestro proyecto?</b>

Triton (pronunciado /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) es la
luna más grande del planeta Neptuno, descubierta el 10 de Octubre de
1846 por William Lassell. Es la única luna grande del Sistema Solar
con una órbita retrógrada, esto es, que orbita en dirección opuesta
a la rotación del planeta.  Con 2.700 km de diámetro. es la séptima luna
en tamaño del Sistema Solar. Triton cuenta con más del 99.5 porciento
de toda la masa que orbita a Neptuno, incluyendo los anillos del planeta y
veinte lunas conocidas.  Tiene más masa que todas las otras 159 lunas
más pequeñas del Sistema Solar combinadas.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Exportar lista de registros</b>

Puede copiar los registros de cualquier lista con Ctrl + C
y pegarlas en cualquier aplicación con Ctrl + V
  <b>Campos a exportar</b> <b>Campos a importar</b> <b>Opciones</b> <b>Exportes predefinidos</b> <b>Bienvenid@ a Tryton</b>


 Una base de datos con el mismo nombre ya existe.
Intente otro nombre de base de datos. Acceso denegado! Acción Acciones Adicionar Adicionar traducción Agregar _nombres de campos Agregar adjunto a un registro La clave de administración y la confirmación son indispensables para crear una nueva base de datos. Clave de Administración: Todos los archivos Siempre ingorar esta advertencia. !Error de Aplicación! ¿Está seguro de querer eliminar este registro? ¿Está seguro que desea eliminar estos registros? Adjunto(%d) Adjunto(0) Adjunto: Auto-Detectar Copia Copia de base de datos Copiar la base de datos elegida. Cuerpo: Inferior Seguimiento de Errores CC: Parámetros CSV C_ambiar C_onectar C_rear No se pudo crear la base de datos, la causa es desconocida.
Si hay una base de datos creada, podría estar dañada.  ¡Pruebe desechar la base de datos! Revise los mensajes de error en busca de información adicional.
Mensaje de error:
 Cancelar conexión al servidor Tryton Cambiar teclas rápidas Verificar actualización automática de la base de datos después de restaurar una base de datos de una versión previa de Tryton. Elija un Plugin Elija una base de datos Tryton para copiar: Elija la base de datos Tryton a eliminar: Elija una clave para el usuario administrador de la nueva base de datos. Con estas credenciales podrá ingresar posteriormente a la base de datos:
Nombre de usuario: admin
Clave: <La clave que establezca> Escoja el idioma predeterminado que se instalará para esta base de datos. Podrá instalar nuevos idiomas después de la instalación a través del menú de administración. Elija el nombre de la base de datos a restaurar.
Los caracteres permitidos son alfanuméricos o _(raya al piso)
!Evite todos los acentos, espacios, virgulillas o caracteres especiales! 
Ejemplo: tryton Elija un nombre para la nueva base de datos.
Se permite caracteres alfanuméricos o _ (raya al suelo)
!Evite cualquier acento, virgulilla, espacio o caracteres especiales! Ejemplo: tryton Limpiar Cerrar Pestaña Línea de órdenes: Comparar Excepción de Concurrencia Confirmación de clave de admin: Confirmación Conectar al servidor Tryton Error de conexión!
Nombre de usuario o clave erróneos! Error de conexión!
Imposible conectarse al servidor! Copiar texto seleccionado No se pudo conectar al servidor! Crear un nuevo registro Crear nueva base de datos Crear nueva relación Crear la base de datos. Se creó un reporte de fallo con ID Fecha de Creación: Usuario Autor: Clave actual: Cortar texto seleccionado _Base de datos !Se hizo copia de seguridad de la base de datos exitósamente! La eliminación de la base de datos falló con el mensaje de error:
 Eliminación de base de datos fallida! La base de datos se eliminó exitósamente! La extracción de datos de la base falló con el error:
 !Extracción de datos de la base fallido! !La base de datos está protegida por contraseña! Nombre de base de datos: La restauración de la base de datos falló con el error:
 !La restauración de la base de datos falló! !La base de datos se restauró exitósamente! Base de datos: Selección de Fecha Selección de Hora Atajos de campos Fecha/Hora Idioma predeterminado: Borrar Borrar Exporte Borrar una base de datos Eliminar registro seleccionado Borrar la base de datos elegida. _Eliminar Base de datos... E-Mail report Editar acciones sobre Archivos Editar preferencias de usuario Editar registro seleccionado Edición de controles Correo Electrónico Configuración de Programa de Correo Electrónico Codificación: Error Error al crear la base de datos! Error al abrir archivo CSV Error al tratar de importar este registro:
%s
Mensaje de Error:
%s

%s Error: Excepción: Exportar a CSV Falso Nombre del campo No se encontró el archivo "%s"  Acciones sobre Archivos Tipo de Archivo _Acciones de archivos... Archivo a importar: Archivo a restaurar: Encontrar Ir al ID Ir al ID: Altura: Host / Database information Hora: ID: Tamaño de Imagen !Tamaño de Imagen muy grande! Imágenes Importar desde CSV ¡Error Importante! ¡Formulario inválido! No es posible hacer copia de una base de datos protegida por contraseña.
La copia y la restauración tienen que hacerse manualmente. Es imposible restaurar una base de datos protegida con contraseña.
La copia y restauración de la base de datos será manual. Atajos de teclado Última fecha de modificación: Última modificación por: Launch action Izquierda Palabras clave disponibles: Límite: Líneas a Omitir: Enlace Usuario M Manage Shortcut Mark line for deletion Minuto: Clave de admin sin diligenciar! Modelo: N Name Error de Red! New Nombre de Nueva Base de Datos: Nueva configuración de base de datos: Siguiente Registro Siguiente Control Siguiente No Sin Atributo de Cadena. !No definió una acción! !No hay un plugin disponible para este recurso! Sin conexión! No se encontró base de datos, ¡Debe crear una! !No hay otro idioma disponible! !No ha seleccionado un registro! !Nada para imprimir! Abrir Abrir una copia de Restauración... Abrir un registro Abrir el calendario Abrir... Abrir/Buscar relación Operación fallida!
Mensaje de error:
%s Operación en progreso Imagen PNG (*.png) Clave: Las claves no coinciden! Pegar texto seleccionado Por favor espere,
esta operación puede tomar un tiempo... Puerto: Preferencias Preferencias Anterior Registro Anterior Control anterior Imprimir Imprimir Diagrama de Flujo Imprimir Diagrama de Flujo (Complejo) Print report Profile Profile Editor Profile: ¡Registro guardado! !Registros NO eliminados! !Registros eliminados! Relación de atajos _Recargar Eliminar Reporte de Error Reportes Solicitudes (%s/%s) Restaurar Restaurar Base de datos Restaurar la base de datos de un archivo. Derecha Conexión SSL Grabar Guardar Como Guardar Como... Guardar Exporte Save Tree Expanded State Guardar Ancho/Alto Guardar este registro Buscar Buscar / Abrir un registro Search Limit Settings Search Limit... Buscar un registro Security risk! Seleccione su acción Selección Conexión al Servidor: Configuración del Servidor: Conexión con el Servidor: Servidor: Configurar la conexión con el servidor... Show plain text Contraseña incorrecta para el servidor Tryton. Por favor intente de nuevo. Corrección Ortográfica Barra de Estado Asunto: Modificador Cambiar Vista Posición de las Pestañas Delimitador de Texto: Atajos en campos de Texto El nombre de la base de datos está restringido a caracteres alfanuméricos y "_" (raya al piso). Evite todos los acentos, virgulillas, espacios y cualquier otro caracter especial. The following action requires to close all tabs.
Do you want to continue? La nueva contraseña de administrador no coincide con el campo de confirmación.
 El mismo error ya había sido reportado por otro usuario.
Para mantenerle informad@ adicionamos su usuario  a la lista de interesados en este asunto The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. Esta versión de cliente no es compatible con el servidor! El nombre de la base de datos ya existe! Esta es la clave del servidor Tryton. No corresponde a un usuario real. Esta clave usualmente se define en la configuración trytond. Este registro ha sido modificado
¿desea guardarlo? Consejos A: Too much arguments Superior Traducir vista Verdadero Conexión a Tryton Clave del Servidor Tryton: Teclee la clave de Admin de nuevo Imposible establecer la localización %s !Imposible escribir el archivo de configuración %s! Desconocido Unmark line for deletion Actualizar Base de datos: Use "%s" para el nombre del archivo Nombre de usuario: Usuario: Ver Bi_tácoras... Esperando solicitudes: %s recibida(s) - %s enviada(s) What is the name of this export? Ancho: Asistente !Trabajando ahora en el(los) registro(s) duplicado(s)! Guardar de todas formas Lo lamentamos, la clave del servidor Tryton no luce correcta.
Intente de nuevo por favor. Clave de servidor Tryton errónea.
Por favor intente de nuevo. Caracteres erróneos en el nombre de la base de datos! !Ícono inválido para el botón! A Sí Va a eliminiar una base de datos de Tryton.
¿Está segur@? No puede ingresar al sistema!
Verifique que tiene un menú definido para su usuario. Puede usar operadores especiales:
* + para incrementar la fecha
* - para decrementar la fecha o limpiar
* = para establecer la fecha o colocar la actual

Las variables disponibles son:
h para horas
d para días
w para semanas (solamente con +/-)
m para meses
y para años

Ejemplos:
"+21d" incrementa la fecha en 21 días
"=11m" establece la fecha al mes 11 del año
"-2w" decrementa en 2 semanas la fecha !Debe elegir un registro! !Debe seleccionar un registro para usar la relación! ¡Debe seleccionar el archivo a importar primero! !Debe guardar el registro antes de adicionar traducciones! Su selección: _Acerca de... Acci_ones... _Agregar _Copia de seguridad de Base de datos... _Cancelar _Cerrar Pestaña C_onectar... pre_determinado _Borrar... _Desconectarse _Desplegar un consejo la próxima vez _Duplicar _Correo Electrónico... _Ejecutar un Plugin E_xportar Datos... _Archivo _Formulario Ir a_l registro ID... _Ayuda _Inicio Í_conos Impor_tar Datos... Atajos de _Teclado... _Manage profiles _Menu Toggle Barra de _Menú _Modo _Nuev@ _Nueva Base de datos... Sig_uiente _Normal _Opciones _PDA _Plugins _Preferencias... _Anterior Im_primir... _Salir... _Leer mis Solicitudes _Recargar _Recargar/Deshacer _Eliminar _Restaurar Base de datos... _Grabar _Guardar Opciones _Enviar solicitud _Atajos Cambiar _Vista _Texto Texto _e Íconos Conse_jos... Barra de _Herramientas _Usuario d h Guardar todo en la bitácora a nivel INFO m Especifique un archivo de configuración alterno Especificar canales para bitácora Especifique el nivel de bitácora(log): INFO, DEBUG, WARNING, ERROR, CRITICAL Especifique el usuario Especifique el nombre del servidor Especifique el puerto del servidor s y 