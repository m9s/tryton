��    �     L              |     }     �     �     �     �     �     �     �     �  �  �  R   �  x   �     ^     v     �     �     �  �   �  H   �                          $     1     A     R  F   r     �  	   �     �     �  #     %   &     L     [     i     u     �     �     �     �     �     �     �     �     �     �     �  1  �  �   .!  &   �!     %"  a   9"     �"  #   �"  #   �"  �   �"  �   �#  �   R$  �   %     �%  	   �%     �%     �%     �%     �%     &     &  +   +&  2   W&     �&     �&     �&     �&     �&     �&     	'     '     -'     F'     _'     n'     }'     �'  	   �'     �'  )   �'     �'     (  )   *(     T(     j(     �(  ,   �(     �(     �(  	   �(     	)     )     ,)     L)     ^)     e)     s)     �)     �)     �)     �)     �)     �)     �)     	*     *     .*     4*  	   K*     U*     [*     t*  &   �*  <   �*     �*  
   �*     +     +     +     .+  
   ?+     J+     ^+  	   k+     u+     �+     �+     �+     �+  	   �+     �+     �+     �+  	   �+     �+     �+  
   �+     ,     ,     %,     5,  "   H,     k,  i   y,  l   �,     P-     c-     }-     �-     �-  !   �-     �-     �-     �-     �-     �-     .     .     
.     .     1.     9.     Q.     X.     Z.     _.     n.     r.     �.     �.     �.     �.     �.     �.     �.  &   �.     /  '   /     :/     W/     k/     }/     �/     �/     �/     �/     �/     �/     �/     �/  #   0     10     G0  	   Y0     c0     |0  /   �0     �0  
   �0     �0     �0     �0     �0     1     
1     1     21     ?1     G1     V1     _1     m1     �1     �1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     2     '2     -2     <2     A2  
   I2     T2     `2     y2     �2     �2     �2     �2     �2     �2     �2     �2     3     #3  	   63     @3     S3     a3     t3     |3     �3  >   �3     �3  	   �3     4     4     4     4     -4     =4  �   T4  I   �4  =   .5  }   l5  �   �5  6   x6  !   �6  �   �6  �   t7  �   8  6   �8     �8  4   �8     9     9     $9     (9     79     <9     N9     f9     �9     �9     �9     �9     �9     �9  +   :  
   3:     >:  	   D:     N:  '   \:      �:     �:     �:  (   �:     �:  .   �:  /   ;  "   H;     k;     �;     �;  J   �;  P   �;  k  (<     �=  -   �=  %   �=  7   >     ?>  	   O>     Y>     e>     j>     ~>  
   �>     �>     �>  
   �>     �>     �>  
   �>  
   �>  	   �>     �>     ?     ?     "?     (?     <?     B?     H?     O?     _?     v?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?  	   �?  	   @     @     @  
   *@     5@     =@     J@  
   R@     ]@     r@     x@     �@  
   �@     �@     �@     �@     �@     �@     �@     �@     �@      �@     A     A     !A  <   9A     vA     �A     �A     �A     �A     �A     �A  �  �A     PC     UC     qC     �C     �C  
   �C     �C     �C     �C  �  �C  R   ~F  �   �F     vG     �G     �G  !   �G     �G  "  �G  _   I     }I     �I     �I     �I     �I     �I     �I  *   �I  _   J     zJ     �J  #   �J     �J  9   �J  :   K     OK     _K     nK     |K  
   �K      �K  (   �K     �K     �K     �K     L     	L     L  
   "L     -L  M  5L  �   �M  &   lN     �N  �   �N     ;O  -   PO  +   ~O  �   �O  �   �P  �   NQ  �   )R     S     	S  
   S     $S     -S  !   HS     jS     wS  A   �S  =   �S     T  '   4T  '   \T     �T     �T  $   �T     �T     �T  $   U  *   -U     XU     lU     �U     �U     �U  7   �U  M   V  3   SV  5   �V  K   �V  2   	W  8   <W     uW  N   �W  3   �W  5   X     KX     ^X  #   tX  %   �X     �X     �X     �X     �X  (   Y  &   ?Y  "   fY     �Y     �Y  #   �Y  %   �Y  &   �Y     Z     %Z     +Z     ?Z     HZ  )   OZ  !   yZ  1   �Z  L   �Z  	   [     $[     0[     @[  0   E[     v[     �[     �[     �[     �[     �[     �[     \     $\     -\     ;\  	   K\     U\     b\     j\     r\     z\     \     �\     �\     �\     �\  !   �\     ]  �    ]  �   �]     [^      n^     �^     �^     �^     �^     �^     �^     �^     _     _  	   _     &_     (_  !   ?_     a_     j_  	   �_     �_     �_     �_     �_  %   �_  /   �_     `     `     $`     3`     7`     N`  /   f`     �`  ;   �`     �`  $   a     *a     =a  #   Da     ha      �a     �a     �a     �a  	   �a     �a  .   b     3b     Gb     Yb  (   hb     �b  4   �b     �b     �b     �b     �b     c     &c     9c     Bc     Wc     vc     �c     �c     �c     �c      �c     �c     d     d  	   -d     7d     ?d     Gd     Zd     cd  	   ud     d  0   �d     �d     �d     �d     �d     �d     e  "   %e     He     ae  	   {e  %   �e     �e     �e     �e     �e     f     %f     Af  
   \f     gf     ~f     �f  	   �f  "   �f     �f  O   �f     Cg     [g     ig     qg     zg     �g     �g     �g  �   �g  O   �h  I   i  �   Vi  �   �i  >   �j  #   �j  �   k  �   �k  �   [l  @   �l     3m  @   ;m     |m     �m     �m     �m     �m     �m      �m  '   �m  (   n  5   1n  (   gn     �n  $   �n  $   �n  2   �n     o     )o     7o     Ko  3   ]o     �o  	   �o     �o  3   �o     �o  ;   �o  ;   ;p  5   wp      �p     �p     �p  X   �p  m   -q  �  �q  ,   Gs  F   ts  6   �s  I   �s     <t     Ot     ]t     it  $   rt     �t     �t     �t     �t     �t     �t  /   �t  
   u  	   u  	   )u     3u     Iu     `u     iu     uu     �u     �u     �u     �u     �u     �u     �u     v     v     +v     1v     :v     Xv     av     iv     rv     wv     �v     �v     �v     �v     �v     �v  
   �v     �v     �v     �v  "   w     .w     6w     Jw     _w     kw     {w     �w     �w     �w     �w     �w     �w     �w     �w  1   �w     x  B   2x  !   ux     �x     �x     �x     �x     �x     �x    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : :  <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 <b>Write Concurrency Warning:</b>

This record has been modified while you were editing it.
 Choose:
    - "Cancel" to cancel saving;
    - "Compare" to see the modified version;
    - "Write Anyway" to save your current version. A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Shortcut Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can not connect to the server!
1. Try to check if the server is running.
2. Find out on which address and port it is listening.
3. If there is a firewall between the server and this client, make sure that the server address and port (usually 8000) are not blocked.
Click on 'Change' to change the address. Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Could not connect to the server Create Create a new record Create new database Create new line Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error processing the file at field %s. Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Fetching databases list Field Separator: Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Host: Hostname: Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Incompatible version of the server Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit Limit: Lines to Skip: Link List Entries Shortcuts Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open related records Open relation Open report Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relate Relation Entries Shortcuts Reload Remove Report Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select a File... Select an Image... Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the URL of the Tryton server. Use server 'localhost' and port '8000' if the server is installed on this computer. Click on 'Change' to change the address. This is the URL of the server. Use server 'localhost' and port '8000' if the server is installed on this computer. Click on 'Change' to change the address. This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To report bugs you must have an account on <u>%s</u> To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Undelete selected record Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: Username: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _E-Mail... _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Reload _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Relate... _Reload _Reload/Undo _Remove _Report... _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port true w y yes Project-Id-Version: tryton 0.0.1
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2008-07-03 23:25+0200
PO-Revision-Date: 2011-10-22 15:40+0200
Last-Translator: Cédric Krier <ced@b2ck.com>
Language-Team: fr_FR
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  de  %d enregistrement importé! %d enregistrement sauvé! %d enregistrements importés! %d enregistrements sauvés! Fichier %s  : : <b>Tout les champs</b> <b>Connaissez-vous Triton, un des homonymes de notre Projet?</b>

Triton est la plus grande lune de la planète Neptune.
Découverte le 10 octobre 1846 par William Lassell, C'est la
seule grande lune dans le système solaire avec une orbite rétrograde, c'est 
à dire une orbite dans la direction opposée à celle de la planète. Avec 2700
km de diamètre, c'est la septième plus grande lune du système solaire.
Triton comprend plus de 99,5 pourcent de tout la masse connue en orbite
autour de Neptune, incluant les anneaux et douze autres lunes. Elle
est aussi plus massive que toutes les 159 plus petites lunes connues du
système solaire combinées.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Exporter la liste d'enregistrements</b>

Vous pouvez copier les enregistrements d'une liste avec Ctrl + C
et les coller dans une autre application avec Ctrl + V
 <b>Champs à exporter</b> <b>Champs à importer</b> <b>Options</b> <b>Exportations prédéfinies</b> <b>Bienvenue sur Tryton</b>


 <b>Attention, accès en écriture concurrents:</b>

Cet enregistrement a été édité pendant que vous étiez en train de le modifier.
  Choisissez:
   - "Annuler" pour annuler vos modifications;
   - "Comparer" pour voir la nouvelle version;
   - "Écraser" pour sauver vos modifications. Une base de données avec le même nom existe déjà.
Essayer un autre nom de base de données. Accès refusé ! Actions Actions Ajouter Ajouter un raccourcis Ajouter une traduction Ajouter les noms des champs Ajouter un attachement à l'enregistrement Le mot de passe admin et de confirmation sont requis pour créer une nouvelle base de données. Mot de passe admin : Tous les fichiers Toujours ignorer cet avertissement. Erreur Applicative ! Êtes-vous sûr de vouloir supprimer cet enregistrement ? Êtes-vous sûr de vouloir supprimer ces enregistrements ? Attachement(%d) Attachement(0) Attachement : Détection automatique Sauvegarde Sauvegarder une base de données Sauvegarder la base de données choisie. Corps : Dessous Bug Tracker CC : Paramètres CSV C_hanger C_onnecter C_réer Impossible de se connecter au serveur !
1. Vérifiez si le serveur est en train de tourner.
2. Trouvez sur quelle adresse et port il écoute.
3. Si il y a un firewall entre le serveur et ce client, vérifiez que l'adresse du serveur et le port (habituellement 8000) ne sont pas bloqués.
Cliquez sur 'Changer' pour changer l'adresse. Impossible de créer une base de données pour une raison inconnue.
Si une base de données a été créée, elle pourrait être inutilisable. Supprimer la ! Vérifiez le message d'erreur pour plus d'information.
Message d'erreur :
 Annuler la connexion au serveur Tryton Changer les raccourcis Cocher pour une mise à jour automatique de la base de données après restauration d'une base de données d'une version précédente de Tryton. Choisissez un plugin Choisissez la base de données à sauvegarder Choisissez la base de données à supprimer Choisissez un mot de passe pour l'utilisateur admin de la nouvelle base de données. Avec ce mot de passe, vous pourrez ensuite vous connecter à la base de données :
Nom d'utilisateur : admin
Mot de passe : <Le mot de passe que vous avez choisi> Choisissez la langue par défaut qui sera utilisée pour cette base de données. Vous pourrez installer d'autres langues après l'installation sous le menu administration. Choisissez le nom de la base de données à restaurer.
Seul sont permis les caractères alphanumériques ou _ (caractère souligné)
Vous devez proscrire les accents, espaces ou caractères spéciaux !
Example : tryton Choisissez le nom de la nouvelle base de données.
Seuls sont permis les caractères alphanumériques ou _ (caractère souligné)
Vous devez proscrire les accents, espaces ou caractères spéciaux ! Example : tryton Effacer Fermer l'onglet Commande : Comparer Erreur d'accès concurrent Confirmer le mot de passe admin : Confirmation Se connecter au serveur Tryton Erreur de connexion !
Mauvais nom d'utilisateur ou mot de passe ! Erreur de connexion !
Impossible de se connecter au serveur ! Copier le texte sélectionné Impossible de se connecter au serveur ! Impossible de se connecter au serveur ! C_réer Créer un nouvel enregistrement Créer une nouvelle base de données Créer une nouvelle ligne Créer une nouvelle relation Créer une nouvelle base de données Un nouveau bogue a été créé avec l'ID  Date de création : Créé par l'utilisateur : Mot de passe actuel : Couper le texte sélectionné _Base de données La base de données a été sauvegardée avec succès ! La suppression de la base de données a échouée avec le message d'erreur :
 La suppression de la base de données a échouée ! La base de données a été supprimée avec succès ! La sauvegarde de la base de donnée a échouée avec le message d'erreur :
 La sauvegarde de la base de données a échouée ! La base de données est protégée par un mot de passe ! Nom de la base de données : La restauration de la base de données a échouée avec le message d'erreur :
 La restauration de la base de données a échoué ! La base de données a été restaurée avec succès ! Base de données : Sélection de la date Sélection de la date et de l'heure Raccourcis des champs Date/Date Heure Langue par défaut : Suppression Supprimer l'exportation Supprimer une base de données Supprimer l'enregistrement sélectionné Supprimer la base de données choisie. Su_pprimer une base de données... Email Rapport par email Editer les actions sur les fichiers Éditer les préférences utilisateur Éditer l'enregistrement sélectionné Widgets d'édition Email Paramètres d'email Codage : Erreur Erreur de création de base de données ! Erreur d'ouverture du fichier CSV Erreur lors du traitement du fichier du champ %s. Erreur à l'importation de cet enregistrement :
%s
Message d'erreur :
%s

%s Erreur :  Exception : Exporter en CSV Faux Récupération de la liste des bases de données Séparateur de champs : Nom du champ Fichier "%s" non trouvé Action sur les fichiers Type de fichier _Actions sur les fichiers... Fichier à importer Fichier à restaurer : Chercher Aller à l'ID Aller à l'ID : Hauteur : Hôte / Port Hôte : Hôte : Heure : ID : Taille de l'image Taille de l'image trop grande ! Images Importer depuis un CSV Erreur d'importation ! Version du serveur incompatible ! Formulaire non-valide ! Il n'est pas possible de sauvergarder une base de données protégée par un mot de passe.
La sauvegarde et la restauration doivent être faites manuellement. Il n'est pas possible de restaurer une base de données protégée par un mot de passe.
La sauvegarde et la restauration doivent être faites manuellement. Raccourcis clavier Date de dernière modification : Dernière modification par : Lancer une action Gauche Légende des symboles : Limite Limite : Lignes à ignorer : Lien Raccourcis des listes Connexion M Gestion des raccourcis Marquer la ligne pour suppression Minute : Mot de passe admin manquant ! Modèle : N Nom Erreur réseau Nouveau Nom de la nouvelle base de données : Configuration de la nouvelle base de données : Suivant Enregistrement suivant Widget suivant Non Pas d'attribut string. Pas d'action définie ! Pas de plugin disponible pour cette ressource ! Pas de connexion ! Pas de base de donnée trouvée, vous devez en créer une ! Pas d'autre langue disponible ! Pas d'enregistrement sélectionné ! Rien à imprimer ! Ouvrir Ouvrir le Fichier de Sauvergarde... Ouvrir un enregistrement Ouvrir les enregistrements liés Ouvrir une relation Ouvrir un rapport Ouvrir le calendrier Ouvrir... Ouvrir/Chercher une relation Échec de l'opération !
Message d'erreur :
%s Opération en cours image PNG (*.png) Mot de passe : Les mots de passe ne correspondent pas ! Coller le texte copié Patientez,
cette opération peut prendre du temps... Port : Préférence Préférences Précédent Enregistrement précédent Widget précédent Imprimer Imprimer le workflow Imprimer le workflow (Complet) Imprimer un rapport Profile Éditeur de profile Profile: Enregistrement sauvé ! Enregistrements non supprimés ! Enregistrements supprimés ! Relation Raccourcis des champs relation Recharger Enlever Rapport Rapporter un bogue Rapports Requêtes (%s/%s) Restaurer Restaurer une base de données Restaurer la base de données depuis le fichier. Droite Connexion SSL Sauver Enregistrer sous Enregistrer sous... Sauver l'exportation Sauver l'état d'arbre développé Sauver largeur / hauteur Sauver cet enregistrement Recherche Rechercher / Ouvrir un enregistrement Limite de recherch Limite la recherche à ... Chercher un enregistrement Alerte sécurité ! Sélectionner un fichier ... Sélectionner une image ... Sélectionnez votre action Sélection Connexion au serveur : Configuration du serveur : Connexion au serveur : Serveur : Configurer la connexion au serveur Montrer en text clair Désolé, le mot de passe du serveur Tryton est incorrect. Veuillez réessayer. Vérifier l'orthographe Barre d'état Sujet : Basculer Basculer la vue Position des onglets Délimiteur de texte : Raccourcis des champs texte Le nom de la base de données est restreint aux caractères alphanumériques et "_" (souligné). Il doit commencer par une lettre et ne pas dépasser 63 caractères.
Les accents, espaces et autres caractères spéciaux sont proscrit. L'action suivante nécessite de fermer tous les onglets.
Voulez-vous continuer? Le mot de passe admin ne correspond pas au mot de passe de confirmation.
 Le même bogue a été déjà rapporté par un autre utilisateur.
Pour vous garder informé, votre nom d'utilisateur a été ajouté à la liste de suivi de ce bogue L'empreinte digitale du serveur a changé depuis la dernière connection !
Les connections à ce serveur sont annulées tant que son empreinte n'est pas corrigée. Cette version du client n'est pas compatible avec le serveur ! La base de données existe déjà ! Ceci est l'URL du serveur Tryton. Utilisez le serveur 'localhost' et le port '8000' si le serveur est installé sur cette machine. Cliquez sur 'Changer' pour changer l'adresse. Ceci est l'URL du serveur. Utilisez le serveur 'localhost' et port '8000' si le serveur est installé sur cette machine. Cliquez sur 'Changer' pour changer l'adresse. Ceci est le mot de passe du serveur Tryton. Il ne correspond pas à un vrai utilisateur. Ce mot de passe est défini dans la configuration de trytond. Cet enregistrement a été modifié
Voulez-vous le sauvegarder ? Astuces Pour rapporter un bogue vous devez avoir un compte sur <u>%s</u> À : Trop d'arguments Dessus Traduire la vue Vrai Connexion Tryton Mot de passe du serveur Tryton : Entrer le mot de passe admin à nouveau Impossible de sélectionner la locale %s Impossible d'écrire le fichier de configuration %s ! Restaurer l'enregistrement sélectionné Inconnu Démarquer la ligne pour suppression Mettre à jour la base de données : Utiliser "%s" comme symbole pour le nom de fichier Nom d'utilisateur : Utilisateur : Nom d'utilisateur : Voir les _Logs... Requêtes en attente: %s reçue(s) - %s envoyée(s) Quel est le nom de cet export ? Largeur : Wizard Sur les enregistrement(s) dupliqué(s) maintenant ! Écraser Mauvais mot de passe du serveur Tryton.
Essayez à nouveau. Mauvais mot de passe du serveur Tryton.
Essayez à nouveau. Mauvais caractères dans le nom de base de données ! Mauvaise icône pour le bouton ! A Oui Vous allez supprimer une base de données Tryton.
Êtes-vous sûr de vouloir continuer ? Vous ne pouvez pas vous connecter au système !
Vérifiez si vous avez un menu défini sur votre utilisateur. Vous pouvez utiliser les opérateurs spéciaux:
* + pour augmenter la date
* - pour diminuer la date
* = pour choisir une date ou la date courante

Les variables disponibles sont :
h pour les heures
d pour les jours
w pour les semaines (seulement pour +/-)
m pour les mois
y pour les années

Examples :
"+21d" augmente de 21 jours la date
"=23w" mets la date à la 23ième semaine de l'année
"-10m" diminue de 10 mois la date Vous devez sélectionner un enregistrement ! Vous devez sélectionner un enregistrement pour utiliser la relation ! Vous devez d'abord sélectionner un fichier d'import ! Vous devez sauvegarder l'enregistrement avant d'ajouter des traductions ! Votre sélection : _À Propos... _Actions... _Ajouter _Sauvegarder une base de données... A_nnuler _Fermer l'onglet _Connecter... _Défaut Su_pprimer... _Déconnecter _Afficher une nouvelle astuce la prochaine fois _Dupliquer _Email... _Email... _Éxécuter un plugin _Export de données... _Fichier _Formulaire _Aller à l'Enregistrement... _Aide _Accueil _Icônes _Import de données... Raccourcis _Clavier... _Gestionnaire de profiles _Recharger le menu Montre/Cache le _Menu _Barre de Menu _Mode _Nouveau _Nouvelle base de données... Sui_vant _Normal _Options _PDA _Plugins _Préférences... _Précédent _Imprimer... _Quitter... Lire mes requêtes _Relation... _Recharger _Recharger/Annuler _Enlever _Rapport... _Restaurer une base de données... _Sauver _Sauver les options Envoyer une requête _Raccourcis _Changer de vue _Textes _Textes et Icônes _Astuces... _Barre d'outils _Utilisateur j h logger tout au niveau INFO m spécifier un fichier de configuration alternatif spécifier les canaux à logger spécifier le niveau de log: INFO, DEBUG, WARNING, ERROR, CRITICAL spécifier l'utilisateur de login spécifier le nom du serveur spécifier le port du serveur vrai s y oui 