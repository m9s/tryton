��    d     <              \     ]     b     v     �     �     �     �     �  �  �  R   o  x   �     ;     S     k     z     �  H   �     �                         +     <  F   \     �  	   �     �     �  #   �  %        6     E     S     _     k     r     �     �     �     �     �     �     �     �     �  �   �  &   �     �  a   �     S  #   c  #   �  �   �  �   f  �   
   �   �      h!  	   n!     x!     �!     �!     �!     �!     �!  +   �!  2   "     B"     U"     r"     �"     �"     �"     �"     �"     �"     �"     #  	   "#     ,#  )   L#     v#     �#  )   �#     �#     �#     $  ,   $     G$     `$  	   �$     �$     �$     �$     �$     �$     �$     �$     %     %     :%     L%     Z%     m%     �%     �%     �%     �%  	   �%     �%     �%     �%  <   &     B&  
   J&     U&     c&  
   i&     t&     �&  	   �&     �&     �&     �&     �&     �&  	   �&     �&     �&     '     '  
   '     "'     8'     ?'     O'     b'  i   p'  l   �'     G(     Z(     t(     �(     �(  !   �(     �(     �(     �(     �(     �(     �(     �(     )     )     +)     2)     4)     9)     H)     L)     _)     s)     x)     �)     �)     �)     �)  &   �)     �)  '   �)     *     1*     E*     W*     \*     {*     �*     �*     �*  #   �*     �*     �*  	   +     +     '+  /   9+     i+  
   o+     z+     �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     
,     ,     -,     >,     Y,     `,  
   g,     r,     z,     �,     �,     �,     �,     �,     �,     �,  
   �,     �,     �,     -     (-     9-     @-     W-     m-     }-     �-     �-  	   �-     �-     �-     �-     �-     �-     .  >   $.     c.  	   r.     |.     �.     �.     �.     �.     �.  �   �.  I   ]/  =   �/  }   �/  �   c0  6   �0  !   (1  �   J1  6   �1     2     2     2     )2     -2     <2     A2     S2     k2     �2     �2     �2     �2     �2  +   �2  
   3     *3     03  '   >3      f3     �3     �3  (   �3     �3  .   �3  /   �3  "   *4     M4     h4     j4  J   n4  P   �4  k  
5     v6  -   �6  %   �6  7   �6     !7  	   17     ;7     G7     L7     `7  
   h7     s7     7  
   �7     �7     �7  
   �7  	   �7     �7     �7     �7     �7     �7     8     8     8     &8     68     M8     ^8     k8     t8     z8     8     �8     �8     �8     �8     �8     �8  	   �8  	   �8     �8     �8     �8     �8     	9     9     &9     ,9     :9  
   J9     U9     b9     h9     x9     �9     �9     �9     �9      �9     �9     �9     �9  <   �9     *:     A:     ]:     u:     w:  �  y:     <     $<     :<     U<     s<  
   �<     �<     �<  �  �<  R   k?  �   �?     O@     h@     �@  $   �@     �@  ]   �@     2A     BA     JA     SA     [A     oA     �A  d   �A     B     ,B  !   ?B     aB  /   vB  0   �B     �B  
   �B     �B     �B     C  0   C  5   IC     C     �C     �C     �C     �C     �C  	   �C     �C  �   �C  %   �D     �D  �   �D     �E  A   �E  )   �E  �   F  �   �F  �   �G  �   OH     �H     I     I     )I     2I  *   MI     xI     �I  >   �I  <   �I     J     8J     XJ     pJ     �J     �J     �J     �J     �J     K     K     1K  A   @K  ?   �K  %   �K  (   �K  C   L  )   UL  0   L     �L  :   �L  +   M  +   3M     _M     nM     �M     �M     �M     �M     �M     �M     �M      N     :N     UN     cN     �N     �N     �N     �N  2   �N     O     *O  2   0O     cO  Y   �O     �O     �O     �O     �O     P  %   P     ;P     SP     cP     |P     �P  	   �P     �P  	   �P     �P     �P     �P     �P     �P  %   Q  	   +Q     5Q     HQ     YQ  �   nQ  �   �Q     {R     �R     �R     �R  	   �R  &   �R     S     S     (S     /S     7S     9S     IS     `S  &   hS     �S     �S     �S     �S     �S  !   �S  )   �S  	   �S     T     T     *T  "   -T     PT  6   oT     �T  5   �T     �T  #   U     /U     IU  +   OU     {U     �U     �U     �U  )   �U     �U     V     V     "V     @V  :   YV     �V     �V     �V     �V     �V     �V     �V     �V  $   W     *W     7W     ?W     NW     WW     iW     �W     �W     �W     �W     �W     �W     �W  	   �W     �W  )   X     @X     HX     VX     ^X     kX     {X     �X     �X     �X     �X     �X     �X     
Y     Y     -Y     <Y  
   RY     ]Y     tY     �Y  	   �Y  *   �Y     �Y  G   �Y     9Z     RZ     bZ     jZ     xZ     �Z     �Z     �Z  �   �Z  I   w[  R   �[  �   \  �   �\  :   /]  ,   j]  �   �]  3   (^     \^     e^     k^     ~^     �^  	   �^     �^      �^  0   �^  '   _  8   -_     f_     r_     �_  2   �_     �_     �_     �_  1   `      7`     X`  	   a`  /   k`     �`  E   �`  F   �`  F   @a  %   �a     �a     �a  O   �a  T   b  �  Xb     �c  3   d  )   9d  6   cd     �d     �d     �d     �d  0   �d  	   �d     e     e     &e  
   6e     Ae  )   Pe  	   ze     �e     �e     �e     �e     �e     �e     �e     �e     f     
f     f     9f     Jf     Wf     gf     mf     tf  
   �f     �f  	   �f     �f     �f     �f  	   �f     �f  	   �f     �f  	   g     g      g     (g     Dg     Mg     _g     qg     �g     �g     �g     �g     �g     �g     �g     �g  -   �g     h  4   h     =h  F   ]h     �h  !   �h  !   �h     �h      i    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Create a new record Create new database Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit: Lines to Skip: Link Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version: tryton 1.4.0
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2009-10-17 11:11+0200
PO-Revision-Date: 2011-10-24 01:05+0200
Last-Translator: Carlos Perelló Marín <carlos@pemas.es>
Language-Team: <tryton-es@googlegroups.com>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  de  %d registro importado Se ha guardado %d registro Se han importado %d registros Se han guardado %d registros archivo %s : <b>Todos los campos</b> <b>¿Conoce Triton? uno de los apelativos de nuestro proyecto</b>

Triton (pronunciado /ˈtraɪtən/ TRYE-tən, o en griego Τρίτων) es la
luna más grande del planeta Neptuno, descubierta el 10 de Octubre de
1846 por William Lassell. Es la única luna grande del Sistema Solar
con una órbita retrógrada, esto es, que orbita en dirección opuesta
a la rotación del planeta.  Con 2.700 km de diámetro. es la séptima luna
en tamaño del Sistema Solar. Triton cuenta con más del 99.5 porciento
de toda la masa que orbita a Neptuno, incluyendo los anillos del planeta y
veinte lunas conocidas.  Tiene más masa que todas las otras 159 lunas
más pequeñas del Sistema Solar combinadas.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Exportar lista de registros</b>

Puede copiar los registros de cualquier lista con Ctrl + C
y pegarlas en cualquier aplicación con Ctrl + V
 <b>Campos a exportar</b> <b>Campos a importar</b> <b>Opciones</b> <b>Exportaciones predeterminadas</b> <b>Bienvenido a Tryton</b>


 Ya existe una base de datos con el mismo nombre.
Inténtelo con otro nombre de base de datos. Acceso denegado Acción Acciones Añadir Añadir traducción Añadir nombres de _campo Agregar un adjunto al registro La contraseña del administrador y su confirmación se necesitan para crear una nueva base de datos. Contraseña del administrador: Todos los archivos Ignorar siempre esta advertencia. Error de aplicación ¿Está seguro que quiere borrar este registro? ¿Está seguro que desea borrar estos registros? Adjunto(%d) Adjunto(0) Adjunto: Auto-detectar Copia de seguridad Realizar copia de seguridad de una base de datos Hacer copia de seguridad de la base de datos elegida. Cuerpo: Inferior Seguimiento de fallos CC: Parámetros CSV C_ambiar C_onectar C_rear No se pudo crear la base de datos, la causa es desconocida.
Si hay una base de datos creada, podría estar dañada. Pruebe a borrar esta base de datos. Revise el mensaje de error en busca de información adicional.
Mensaje de error:
 Cancelar conexión al servidor Tryton Cambiar teclas rápidas Seleccionelo para que se realice una actualización automática de la base de datos desde una versión anterior de Tryton, después de restaurarla. Elija una extensión Elija una base de datos Tryton para hacer una copia de seguridad: Elija la base de datos Tryton a eliminar: Elija una contraseña para el usuario administrador de la nueva base de datos. Podrá acceder a la base de datos con dichas credenciales:
Nombre de usuario: admin
Contraseña: <La contraseña que introduzca aquí> Escoja el idioma predeterminado que se instalará para esta base de datos. Podrá instalar nuevos idiomas después de la instalación, a través del menú de administración. Elija el nombre de la base de datos a restaurar.
Los caracteres permitidos son alfanuméricos o _(subrayado)
Evite todos los acentos, espacios, o caracteres especiales 
Ejemplo: tryton Elija un nombre para la nueva base de datos.
Puede utilizar caracteres alfanuméricos o _ (subrayado)
Evite cualquier acento, espacio o caracteres especiales. Ejemplo: tryton Limpiar Cerrar pestaña Línea de comando: Comparar Excepción de concurrencia Confirme la contraseña del administrador: Confirmación Conectar al servidor Tryton Error de conexión
Nombre de usuario o contraseña incorrectos Error de conexión
No ha sido posible conectarse al servidor Copiar texto seleccionado No se pudo conectar al servidor Crear un nuevo registro Crear nueva base de datos Crear nueva relación Crear la nueva base de datos. Se creó un nuevo fallo con ID  Fecha de creación: Usuario creador: Contraseña actual: Cortar texto seleccionado _Base de datos La copia de seguridad de la base de datos finalizó correctamente El borrado de la base de datos falló con el mensaje de error:
 Falló el borrado de la base de datos La base de datos se borró correctamente La extracción de la base de datos falló con el mensaje de error:
 La extracción de la base de datos falló La base de datos está protegida por contraseña Nombre de la base de datos: La restauración de la base de datos falló con el error:
 La restauración de la base de datos falló La base de datos se restauró correctamente Base de datos: Selección de fecha Selección de hora Atajos de campos Fecha/Hora Idioma predeterminado: Borrar Borrar exportación Borrar una base de datos Borrar registro seleccionado Borrar la base de datos elegida. _Eliminar base de datos... E-Mail report Editar acciones sobre archivos Editar preferencias de usuario Editar registro seleccionado Edición de controles Correo electrónico Configuración del programa de correo electrónico Codificación: Error Se ha producido un error al crear la base de datos Error al abrir el archivo CSV Se ha producido un error al tratar de importar este registro:
%s
Mensaje de Error:
%s

%s Error:  Excepción: Exportar a CSV Falso Nombre del campo El archivo «%s» no se ha encontrado Acciones sobre archivos Tipo de archivo _Acciones de archivos... Archivo a importar: Archivo a restaurar: Encontrar Ir al ID Ir al ID: Altura: Host / Database information Hora: ID: Tamaño de la imagen El tamaño de la imagen es muy grande Imágenes Importar desde CSV Error importante Formulario inválido No es posible hacer copia de una base de datos protegida por contraseña.
La copia de seguridad y su restauración tiene que ser manual. No es posible restaurar una base de datos protegida con contraseña.
La copia de seguridad y su restauración tiene que ser manual. Combinaciones de teclas Última fecha de modificación: Última modificación por: Launch action Izquierda Leyenda de palabras clave disponibles: Límite: Líneas a omitir: Enlace Usuario M Manage Shortcut Mark line for deletion Minuto: Falta la contraseña del administrador Modelo: N Name Error de red New Nombre de la base de datos nueva: Nueva configuración de la base de datos: Siguiente Registro siguiente Siguiente control No No tiene una cadena como atributo. No se definió ninguna acción No hay ninguna extensión disponible para este recurso No hay conexión No se encontró ninguna base de datos, debe crear una No hay otro idioma disponible No ha seleccionado ningún registro No hay nada para imprimir Abrir Abrir una copia de seguridad a restaurar... Abrir un registro Abrir el calendario Abrir... Abrir/Buscar relación Falló la operación
Mensaje de error:
%s Operación en progreso Imagen PNG (*.png) Contraseña: Las contraseñas no coinciden Pegar texto seleccionado Por favor espere,
esta operación puede tomar un tiempo... Puerto: Preferencias Preferencias Anterior Registro anterior Control anterior Imprimir Imprimir flujo de trabajo Imprimir flujo de trabajo (Complejo) Print report Profile Profile Editor Profile: Registro guardado Los registros no se han borrado Registros borrados Relación de atajos Recargar Eliminar Informar de un fallo Informes Solicitudes (%s/%s) Restaurar Restaurar base de datos Restaurar la base de datos de un archivo. Derecha Conexión SSL Guardar Guardar como Guardar como... Guardar exportación Save Tree Expanded State Guardar ancho/alto Guardar este registro Buscar Buscar / Abrir un registro Search Limit Settings Search Limit... Buscar un registro Security risk! Seleccione su acción Selección Conexión al servidor: Configuración del servidor: Conexión con el servidor: Servidor: Configurar la conexión con el servidor... Show plain text La contraseña del servidor Tryton no es correcta. Inténtelo de nuevo. Corrección ortográfica Barra de estado Asunto: Cambiar vista Cambiar vista Posición de las pestañas Delimitador de texto: Atajos en campos de texto El nombre de la base de datos está restringido a caracteres alfanuméricos y "_" (subrayado). Evite todos los acentos, espacios y cualquier otro carácter especial. The following action requires to close all tabs.
Do you want to continue? La nueva contraseña del administrador no coincide con el campo de confirmación.
 El mismo fallo ya fue notificado por otro usuario.
Para mantenerle informado añadiremos su usuario a la lista de interesados en este asunto The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. Esta versión del cliente no es compatible con el servidor Ya existe una base de datos con este nombre. Esta es la contraseña del servidor Tryton. No corresponde a un usuario real. Esta contraseña se suele definir en la configuración de trytond. Este registro ha sido modificado
¿desea guardarlo? Consejos Para: Too much arguments Superior Traducir vista Verdadero Conexión a Tryton Contraseña del servidor Tryton: Teclee la contraseña del administrador de nuevo No se ha podido establecer el idioma %s No se ha podido escribir el archivo de configuración %s Desconocido Unmark line for deletion Actualizar base de datos: Utilice "%s" como referencia al nombre del archivo Nombre de usuario: Usuario: Ver _Registro... Solicitudes en espera: %s recibidas - %s enviadas What is the name of this export? Anchura: Asistente Está trabajando ahora en el registro duplicado Guardar de todas formas La contraseña del servidor Tryton es incorrecta
Inténtelo de nuevo. La contraseña del servidor Tryton es incorrecta.
Inténtelo de nuevo. Ha introducido caracteres incorrectos en el nombre de la base de datos El icono para el botón no es válido A Sí Va a eliminar una base de datos de Tryton.
¿Está seguro que quiere continuar? No puede entrar en el sistema
Verifique que tiene un menú definido para su usuario. Puede usar operadores especiales:
* + para incrementar la fecha
* - para decrementar la fecha o borrarla
* = para establecer la fecha o la fecha actual

Las variables disponibles son:
h para horas
d para días
s para semanas (solamente con +/-)
m para meses
a para años

Ejemplos:
"+21d" incrementa la fecha en 21 días
"=11m" establece la fecha al mes 11 del año
"-2s" decrementa en 2 semanas la fecha Debe elegir un registro Debe seleccionar un registro para usar la relación Primero debe elegir un archivo a importar Debe guardar el registro antes de añadir traducciones Su selección: _Acerca de... _Acciones... _Añadir Hacer _copia de seguridad de la base de datos... _Cancelar _Cerrar pestaña _Conectar... pre_determinado _Borrar... _Desconectarse _Mostrar un nuevo consejo la próxima vez _Duplicar _Correo Electrónico... _Ejecutar una extensión _Exportar datos... _Archivo _Formulario Ir al re_gistro ID... Ay_uda _Inicio _Iconos _Importar datos... Com_binaciones de teclas... _Manage profiles _Menu Toggle Barra de _menú _Modo _Nuevo _Nueva base de datos... Sig_uiente _Normal _Opciones _PDA _Extensiones _Preferencias... _Anterior Im_primir... _Salir... _Leer mis solicitudes _Recargar _Recargar/Deshacer _Borrar _Restaurar base de datos... _Guardar _Guardar opciones _Enviar solicitud Acceso _directo Cambiar vi_sta _Texto Texto _e iconos Conse_jos... Barra de herramien_tas U_suario d h Registrar toda la información con nivel INFO m Especifique un archivo de configuración alternativo Especificar canales a registrar Especifica el nivel de registro: INFO, DEBUG, WARNING, ERROR, CRITICAL Especifica el usuario Especifica el nombre del servidor Especifica el puerto del servidor s y 