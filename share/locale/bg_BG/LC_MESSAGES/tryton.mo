��    d     <              \     ]     b     v     �     �     �     �     �     �  R   �  x         �     �     �     �     �  H        W     f     m     u     y     �     �  F   �       	             7  #   J  %   n     �     �     �     �     �     �     �     �                         +     3     <  �   D  &        ;  a   O     �  #   �  #   �  �   	  �   �  �   h  �        �  	   �     �     �     �               '  +   A  2   m     �     �     �     �     �           %      >      M      \      n   	   �      �   )   �      �      �   )   	!     3!     I!     i!  ,   x!     �!     �!  	   �!     �!     �!     "     +"     ="     D"     R"     d"     {"     �"     �"     �"     �"     �"     �"     #     #  	   ##     -#     3#     L#  <   c#     �#  
   �#     �#     �#  
   �#     �#     �#  	   �#     �#     $     $     /$     4$  	   =$     G$     O$     k$     q$  
   u$     �$     �$     �$     �$     �$  i   �$  l   8%     �%     �%     �%     �%     �%  !   �%     &     &&     5&     :&     @&     B&     R&     i&     q&     �&     �&     �&     �&     �&     �&     �&     �&     �&     �&     �&     �&     '  &   '     ;'  '   J'     r'     �'     �'     �'     �'     �'     �'     �'     (  #   (     :(     P(  	   b(     l(     �(  /   �(     �(  
   �(     �(     �(     �(     �(     )     )     ")     ;)     H)     P)     _)     h)     v)     �)     �)     �)     �)  
   �)     �)     �)     �)     �)     *     "*     (*     7*     <*  
   D*     O*     [*     t*     �*     �*     �*     �*     �*     �*     �*     �*  	   +     +     *+     8+     K+     S+     r+  >   �+     �+  	   �+     �+     �+     �+     �+     ,     ,  �   +,  I   �,  =   -  }   C-  �   �-  6   O.  !   �.  �   �.  6   4/     k/     p/     t/     �/     �/     �/     �/     �/     �/     �/     �/     0     '0     @0  +   Q0  
   }0     �0     �0  '   �0      �0     �0     �0  (   �0     1  .   )1  /   X1  "   �1     �1     �1     �1  J   �1  P   2  k  h2     �3  -   �3  %   !4  7   G4     4  	   �4     �4     �4     �4     �4  
   �4     �4     �4  
   �4     �4     �4  
   5  	   %5     /5     A5     Q5     W5     ]5     q5     w5     }5     �5     �5     �5     �5     �5     �5     �5     �5     �5     �5     �5     6     
6     6  	   #6  	   -6     76     @6     R6     Z6     g6     o6     �6     �6     �6  
   �6     �6     �6     �6     �6     �6     �6     �6     �6      �6     7     7     37  <   K7     �7     �7     �7     �7     �7  �  �7     c9     i9  !   �9  !   �9  #   �9     �9     �9     �9      :  R   #:  �   v:  +   U;  )   �;     �;  G   �;  -   <  �   ;<     �<     �<     �<     =  "   =  2   :=  C   m=  �   �=  -   a>     �>  F   �>  )   �>  M   ?  O   j?     �?     �?     �?  /   @     @@  -   U@  A   �@  	   �@     �@     �@     �@     �@     A     A     %A  �  8A  ;   �B  )   C  �   7C     �C  F   D  C   ID  K  �D  �   �E  W  �F    H     ,I     AI     `I     yI      �I  1   �I     �I  +   �I  k   &J  n   �J  1   K  ;   3K  )   oK  4   �K  -   �K  -   �K  1   *L  !   \L  "   ~L     �L  3   �L     �L  <   M  R   @M  G   �M  6   �M  R   N  G   eN  <   �N     �N  Z   �N  C   MO  N   �O     �O     �O  &   P  D   5P  &   zP     �P  *   �P  ,   �P  3   Q  ?   @Q  .   �Q     �Q  :   �Q  I   �Q  7   BR     zR     �R  3   �R     �R     �R  @   �R  6   $S  t   [S     �S     �S     �S     T     T  &   %T     LT     kT  "   {T     �T  ,   �T     �T     �T     U     )U     ;U     WU     _U  (   cU  H   �U     �U     �U  %   V     ,V  �   KV  �   W     �W  .   �W  '   )X     QX     _X  8   lX     �X  '   �X     �X  !   �X     Y     Y     'Y     >Y  :   LY     �Y     �Y     �Y     �Y     �Y  )   �Y  5   �Y     Z     ,Z     FZ     dZ     iZ  *   �Z  B   �Z     �Z  j   [  *   x[  &   �[  "   �[     �[  T   �[      S\  (   t\     �\  1   �\  Q   �\  ,   5]  "   b]     �]  %   �]  5   �]  _   �]  	   O^     Y^     l^     ^     �^  !   �^  
   �^  +   �^  8   _     >_     K_     S_     b_     k_  "   �_      �_     �_     �_     `  &   `     >`     M`     b`  3   }`  B   �`     �`     a  
   a     a     3a  "   Ka     na  5   �a  #   �a     �a  1   �a     "b     8b     Hb  $   eb  !   �b     �b  #   �b  $   �b  #   c     %c  A   3c     uc  ]   �c  &   �c  $   
d     /d     ?d  *   Xd      �d  #   �d  *   �d  �   �d  I   �e  �   /f  �   �f    �g  b   �h  I   i  �   Zi  V   6j     �j     �j     �j     �j     �j     �j     �j  '   k  P   -k  D   ~k  K   �k     l     $l  .   =l  S   ll  "   �l     �l      �l  J   m  :   fm     �m     �m  @   �m  
   n  \   n  ]   in  G   �n  *   o     :o     =o  y   Bo  �   �o  n  Kp  7   �r  Z   �r  M   Ms  b   �s     �s     t     &t     :t  0   Kt  
   |t     �t     �t     �t     �t     �t  F   u     Lu     _u  *   hu  %   �u     �u  
   �u  !   �u  
   �u     �u  
   v  #   v     6v     Sv     dv     qv  
   �v     �v     �v     �v     �v     �v     �v     �v     �v     w     ,w     :w  /   Hw     xw  '   �w     �w  6   �w  
   x  "   x  $   1x     Vx  *   px  
   �x     �x     �x  $   �x     �x     y     y  8   y     Ly  ?   Oy  :   �y  U   �y  4    z  /   Uz  +   �z     �z     �z    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : :  <b>All fields</b> <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Create a new record Create new database Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit: Lines to Skip: Link Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relation Entries Shortcuts Reload Remove Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Reload _Reload/Undo _Remove _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port w y Project-Id-Version: tryton 1.5.0
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2010-04-07 11:34+0200
PO-Revision-Date: 2011-10-24 01:01+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: bg_BG <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
  от %d запис вмъкнат! %d заспис съхранен! %d записи вмъкнати! %d записа съхранени! %s файла : :  <b>Всички полета</b> <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Извличане на списък записи</b>

Може да копирате записи от всеки списък с Ctrl + C
и да ги поставите във всяка програма с Ctrl + V
 <b>Полета за извличане</b> <b>Полета за вмъкване</b> <b>Настройки</b> <b>Предварително зададени извличания</b> <b>Добре дошли при Tryton</b>


 Вече съществува база данни с това име.
Опитайте с друго име на базата данни. Достъпа отказан! Действие Действия Добавяне Добавяне на превод Добавяне на имена на полета Добавяне на прикачен файл към записа За зъздаване на нова база данни са необходими парола за администратор и парола за потвърждение. Парола на администратор: Всички файлове Винаги игнорирай това предупреждение. Грешка в приложението! Наистина ли искате да изтриете този запис? Наистина ли искате да изтриете тези записи? Прикачен файл(%d) Прикачен файл(0) Прикачен файл: Автоматично разпознаване Архивиране Архивиране на база данни Архивиране на избраната база данни. Тяло: Отдолу Bug Tracker CC: CSV параметри Промяна Свързване Създаване По неизвестна причина не базата данни не може да бъде създадена.
Ако има така създадена база данни вероятно е с грешки. Може да изтерете тази база данни! За повече информация вижте съобщинието за грешка.
Съобщение за грешка:
 Отказ от свръзване с Tryton сървъра Смяна на бързи клавиши Проверка за автоматични обновявания след възтановяване на база данни от предишна Tryton версия. Избор на добавка Изберете Tryton база данни за архивиране. Избор на Tryton база данни за изтриване: Изберете парола за потребителя-администратор на новата база данни. С тези данни по-късно ще може да влезете в базата данни:
Потребителско име: admin
Парола: <Паролата която сте избрали> Изберете езика по подразбиране за тази база данни. Може да добавите след инсталацията друг език през менюто за администрация. Избор на името на базата данни която да бъде възтановена.
Позволени символи са букви, цифри или – (под черта)
Не може да използвате акценти, празен символ или специални символи! 
Пример: tryton Изберете име на новата база данни.
Позволени символи за букви, цифри и _ (под черта)
Избягвайте акценти, празен символ или специални символи! Пример: tryton Изчистване Затваряне на таб Команден ред: Сравняване Грешка при достъп Потвърждаване на паролата: Потвърждение Свързване с Tryton сървъра Грешка при свтрзване!
Грешно потребителско име или парола! Грешка при свързване!
Невъзможност да се свърже със сървъра! Копиране на избрания текст Не може да се свърже със сървъра! Създаване на нов запис Създаване на нова база данни Създаване на нова връзка Създай новата база данни Създадена е нова грешка с ID Дата на създаване: Потребител създал: Текуща парола Изрязване на избрания текст База данни Базата данни архивирана успешно! Изтриването на базата данни неуспя с грешка:
 Изтриването на базата данни неуспешно! Базата данни изтрита успешно! Извличането на базата данни неуспя с грешка:
 Извличането на базата данни неуспешно! Базата данни е защитена с парола! Име: Възтановяването на базата данни неуспя с грешка:
 Възтановяването на базата неуспешно Възтановяването на базата данни неуспешно База данни: Избор на дата Избор на дата и време Бързи клавиши на записи за дата/време Език по подразбиране Изтриване Изтриване на извличане Изтриване на база данни. Изтриване на избрания запис Изтриване на избраната база данни. Изтриване на база данни... E-Mail report Редактиране на действия на файл Редактиране на потребителски настройки Редактиране на избрания запис Edition Widgets Email Настройки на програма за email Кодиране: Грешка Грешка при създаване на база данни! Грешка при отваряне на CSV файл Грешка при вмъкване на следния запис:
%s
Съобшение за грешка:
%s

%s Грешка:  Грешка: Извличане в CSV Лъжа Име на поле Файла "%s" не е намерен Действия на файл Вид файл Действия с файлове Файл за вмъкване: Файл за възтановяването Търсене Отиване на ID Отиване на ID: Височина: Host / Database information Час: ID: Размер на изображение Размера на изображението е много голям! Изображения Вмъкване от CSV Грешка при вмъкване! Невалидна форма! Не е възможно да се извлече база данни защитена с парола.
Архивирането и възтановяването трябва да стане ръчно. Не може да се възтановяви база данни защитена с парола.
Архивирането и възтановяването трябва да стане ръчно. Бързи клавиши Дата на последна промяна: Последно променен от: Launch action Отляво Лагенда за налични контейнери: Ограничение: Редове за пропускане: Връзка Потребителско име М Manage Shortcut Mark line for deletion Минута: Липсва парола за администратор! Модел: N Име Мрежова грешка! New Име на нова база данни: Настройка на нова база данни: Следващ Следващ запис Следваща джаджа Не Няма атрибут низ Не е зададено действие! Няма налична добавка за този ресурс! Няма връзка! Не е намерена нито една база данни, трябва да създете нова! Няма наличен друг език! Не са избрани записи! Няма нищо за печат! Отваряне Отваряне на архивиран файл за възтановяване... Отваряне на запис Отваряне на календара Отваряне... Отваряне/Търсене на връзка Операцията неуспешна!
Съобщение за грешка:
%s Операцията се изпълнява PNG изображение (*.png) Парола: Паролата не съвпада! Поставяне на копирания текст Моля изчакайте,
тази операция може да отнеме време... Порт: Настройки Настройки Предишен Предишен запис Предишенна джаджа Печат Печат на работен процес Печат на работен процес (Пълно) Print report Profile Profile Editor Profile: Записа съхранен! Записа не е изтрит! Записите изтрити! Relation Entries Shortcuts Презареждане Премахване Съобщаване за грешка Справки Заявки (%s/%s) Възтановяване Възтановяване на база данни Възтановяване на база данни от файл. Отдясно SSL връзка Запис Запис като Запис като ... Запис на извличане Save Tree Expanded State Запазване на ширина/височина Запис на този запис Търсене Търсене / Отваряне на запис Search Limit Settings Search Limit... Тъсене на запис Риск за сигурността Изберете действие Избор: Връзка със сървъра: Настройка на сървър Връзка към сървъра: Сървър: Настройка на връзката със сървъра... Show plain text Грешна парола за Tryton сървъра. Моля опитайте отново. Проверка на правопис Лента със състояние Относно: Превключване Превключване на изглед Позиции на табове Разделител на думи: Текстови бързи клавиши Името на базата данни е ограничено до латински букви, цифри и "_" (под черта). Избягвайте акценти, празен символ или специални символи. The following action requires to close all tabs.
Do you want to continue? Новата парола на администратора не съвпада с тази в полето за потвърждение.
 Тази грешка е вече съобщена от друг потребител..
За да бъдете информирани потребителя ви е добавен списъка за този проблем Идентификатора на сървъра е променен след последната връзка!
Приложението ще спре да се връзва към сървъра докато не бъде оправен идентификатора му. Версията на клиента не е съвместима с тази на сървъра! Вече съществува база данни с такова име! Това е паролата на Tryton сървъра. Тя не е на никой потребител. Тя обикновенно се задава по време на конфигурирането на trytond. Този запис е променен
искате ли да го запишете ? Подсказки До Too much arguments Отгоре Превод на изглед Истина Tryton връзка Парола за Tryton сървъра Напишете отново паролата на администратора Не може да зададе локални настройки %s Не може да запише конфигурационен файл %s! Неисвестен Unmark line for deletion Обновяване на база данни: Ползвайте "%s" като контейнер за името на файла Потребителско име: Потребител:  Преглед на логове Очакващи заявки: %s получени - %s изпратени Какво е името на това извличане? Ширина: Помощник Работите върху дублиран(и) запис(и)! Запис Грешна парола за Tryton сървъра
Моля опитайте отново. Грешна парола за Tryton сървъра.
Моля опитайте отново. Грешни символи в името на базата данни! Грешна икона за бутона! Г Да Ще изтерете Tryton база данни.
Сигурни ли сте че искате да продължите? Не може да влезете в системата!
Проверете дали имате меню за вашия потребител. Може да ползвате специални оператори:
* + за увеличаване на датата
* - за намаляване на датата или изчистване
* = за задаване на дата или текущата дата

Възможни променли:
h за часове
d за дни
w за седмици (само с +/-)
m за месеци
y за години

Пример:
"+21d" увеличава датата с 21 дена
"=11m" поставя датата на 11-я месец от годината
"-2w" намалява датата с 2 седмици Трябва да изберете един запис! Трябва за изберете запис за използвате връзката!  Първо трябва да изберете файл за вмъкване! Трябва да съхраните записа преди да добавите преводи! Вашия избор Относно... Действия... Добавяне Архивиране на база данни... Отказ Затваряне на таб Свързване... По подразбиране Изтриване... Изключване Показвай нова подсказка следващия път Дублиране Email... Изпълнаване на добавка Извличане на данни... Файл Форма Отиване на запис ID Помощ Начало Икони Вмъкване на данни... Бързи клавиши... _Manage profiles _Menu Toggle Лента с меню Режим Нов Нова база данни Следващ Нормален Настройки PDA Добавки Предпочитания... Предишен Печат... Изход... Прочитане на моите заявки Презареждане Презареждане/Обратно Изтриване Възтановяване на база данни... Запис Запазване на опции Изпращане на заявка Бързи клавиши Превключване на изглед Текст Текс и икони Подсказки... Лента с инструменти Потребител д ч записване на всичко на ниво INFO  м укажете друг конфигурационен файл указване на канали за записване указване на ниво на запис: INFO, DEBUG, WARNING, ERROR, CRITICAL укажете потребителското име укажете адреса на сървъра укажете порт на сървъра с y 