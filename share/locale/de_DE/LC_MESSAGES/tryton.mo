��    �     L              |     }     �     �     �     �     �     �     �     �  �  �  R   �  x   �     ^     v     �     �     �  �   �  H   �                          $     1     A     R  F   r     �  	   �     �     �  #     %   &     L     [     i     u     �     �     �     �     �     �     �     �     �     �     �  1  �  �   .!  &   �!     %"  a   9"     �"  #   �"  #   �"  �   �"  �   �#  �   R$  �   %     �%  	   �%     �%     �%     �%     �%     &     &  +   +&  2   W&     �&     �&     �&     �&     �&     �&     	'     '     -'     F'     _'     n'     }'     �'  	   �'     �'  )   �'     �'     (  )   *(     T(     j(     �(  ,   �(     �(     �(  	   �(     	)     )     ,)     L)     ^)     e)     s)     �)     �)     �)     �)     �)     �)     �)     	*     *     .*     4*  	   K*     U*     [*     t*  &   �*  <   �*     �*  
   �*     +     +     +     .+  
   ?+     J+     ^+  	   k+     u+     �+     �+     �+     �+  	   �+     �+     �+     �+  	   �+     �+     �+  
   �+     ,     ,     %,     5,  "   H,     k,  i   y,  l   �,     P-     c-     }-     �-     �-  !   �-     �-     �-     �-     �-     �-     .     .     
.     .     1.     9.     Q.     X.     Z.     _.     n.     r.     �.     �.     �.     �.     �.     �.     �.  &   �.     /  '   /     :/     W/     k/     }/     �/     �/     �/     �/     �/     �/     �/     �/  #   0     10     G0  	   Y0     c0     |0  /   �0     �0  
   �0     �0     �0     �0     �0     1     
1     1     21     ?1     G1     V1     _1     m1     �1     �1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     2     '2     -2     <2     A2  
   I2     T2     `2     y2     �2     �2     �2     �2     �2     �2     �2     �2     3     #3  	   63     @3     S3     a3     t3     |3     �3  >   �3     �3  	   �3     4     4     4     4     -4     =4  �   T4  I   �4  =   .5  }   l5  �   �5  6   x6  !   �6  �   �6  �   t7  �   8  6   �8     �8  4   �8     9     9     $9     (9     79     <9     N9     f9     �9     �9     �9     �9     �9     �9  +   :  
   3:     >:  	   D:     N:  '   \:      �:     �:     �:  (   �:     �:  .   �:  /   ;  "   H;     k;     �;     �;  J   �;  P   �;  k  (<     �=  -   �=  %   �=  7   >     ?>  	   O>     Y>     e>     j>     ~>  
   �>     �>     �>  
   �>     �>     �>  
   �>  
   �>  	   �>     �>     ?     ?     "?     (?     <?     B?     H?     O?     _?     v?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?     �?  	   �?  	   @     @     @  
   *@     5@     =@     J@  
   R@     ]@     r@     x@     �@  
   �@     �@     �@     �@     �@     �@     �@     �@     �@      �@     A     A     !A  <   9A     vA     �A     �A     �A     �A     �A     �A  �  �A     aC     gC     �C     �C     �C  
   �C     �C     �C     �C  �  �C  a   �F  �   G     �G     �G     �G     H     !H  �  AH  ^   �I     *J     =J     DJ     MJ     YJ     mJ     �J  &   �J  z   �J     ?K     WK  *   dK     �K  0   �K  1   �K     L  	   L     L     #L     :L     BL  "   TL     wL     �L     �L     �L     �L     �L  
   �L  
   �L  �  �L  H  �N  	   �O     �O  �   P     �P  @   �P  @   Q  �   VQ  �   PR  �   �R  �   �S     T     �T     �T     �T     �T     �T     �T  '   �T  8   U  3   MU     �U      �U      �U  	   �U     �U     V     V     1V     JV  $   cV     �V     �V     �V     �V  
   �V      �V  O   W  (   QW      zW  A   �W  (   �W  !   X     (X  J   .X  3   yX  .   �X  
   �X     �X     �X  '   Y     *Y     <Y     EY     UY      hY  #   �Y     �Y     �Y     �Y     �Y      �Y  "   Z     BZ     ZZ     aZ  
   vZ     �Z  #   �Z  !   �Z  1   �Z  <    [     =[  
   E[     P[     d[      k[     �[     �[     �[     �[  	   �[     �[     �[     �[     \     \     *\     ;\     B\     Y\  	   _\     i\     q\     u\     �\     �\     �\     �\     �\     �\  �    ]  �   �]     6^     D^     Z^     q^     �^  %   �^     �^     �^     �^     �^  ,   �^  	   _     !_     #_     7_     T_     \_     z_     �_     �_     �_     �_     �_     �_  	   �_     �_     �_     �_     `     `  +   /`     [`  >   m`  "   �`     �`     �`     �`  .   a     5a  !   Ga     ia     {a     �a  
   �a     �a  +   �a     �a     b  	   b  !    b     Bb  4   _b     �b     �b     �b  
   �b     �b     �b     �b     �b     �b     c     ,c     3c     Ec     Mc     dc     �c  	   �c  4   �c  	   �c  	   �c     �c     �c     d     d     d     /d  $   Jd     od     vd  	   �d     �d     �d     �d     �d     �d     �d     e     e     1e     Me     `e     qe     �e     �e     �e     �e     �e     �e     �e     f  !   f     -f  `   ?f     �f     �f     �f     �f     �f     �f      g      g  �   3g  E   h  Q   bh  �   �h  �   Ti  ?   j  2   Kj  �   ~j  �   `k  �   Bl  I   m     am  M   gm     �m     �m     �m     �m     �m  
   �m     �m     n     �n  .   �n  ;   �n  	   o  $   %o     Jo  6   co     �o     �o     �o     �o  ,   �o  %   p     *p     2p  /   9p     ip  S   xp  I   �p  /   q      Fq     gq     iq  ?   lq  u   �q  `  "r  2   �s  T   �s  0   t  T   <t     �t  	   �t     �t     �t     �t  
   �t     �t     �t  	   
u     u     !u  %   5u     [u     hu  
   u     �u     �u     �u  	   �u     �u     �u     �u     �u     �u     v     !v     4v     Ev     Wv     dv     kv     pv  
   �v     �v     �v     �v     �v     �v     �v     �v     �v     �v     w  
   w     (w  
   @w     Kw     _w  
   ~w     �w     �w  
   �w     �w     �w     �w  	   �w     �w  	   x     x     x     x     +x  '   -x     Ux  8   ix     �x     �x     �x     �x     �x     �x     �x    of  %d record imported! %d record saved! %d records imported! %d records saved! %s file : :  <b>All fields</b> <b>Do you know Triton, one of the namesakes for our project?</b>

Triton (pronounced /ˈtraɪtən/ TRYE-tən, or as in Greek Τρίτων) is the
largest moon of the planet Neptune, discovered on October 10, 1846
by William Lassell. It is the only large moon in the Solar System
with a retrograde orbit, which is an orbit in the opposite direction
to its planet's rotation. At 2,700 km in diameter, it is the seventh-largest
moon in the Solar System. Triton comprises more than 99.5 percent of all
the mass known to orbit Neptune, including the planet's rings and twelve
other known moons. It is also more massive than all the Solar System's
159 known smaller moons combined.
 <b>Export graphs</b>

You can save any graphs in PNG file with right-click on it.
 <b>Export list records</b>

You can copy records from any list with Ctrl + C
and paste in any application with Ctrl + V
 <b>Fields to export</b> <b>Fields to import</b> <b>Options</b> <b>Predefined exports</b> <b>Welcome to Tryton</b>


 <b>Write Concurrency Warning:</b>

This record has been modified while you were editing it.
 Choose:
    - "Cancel" to cancel saving;
    - "Compare" to see the modified version;
    - "Write Anyway" to save your current version. A database with the same name already exists.
Try another database name. Access denied! Action Actions Add Add Shortcut Add Translation Add _field names Add an attachment to the record Admin password and confirmation are required to create a new database. Admin password: All files Always ignore this warning. Application Error! Are you sure to remove this record? Are you sure to remove those records? Attachment(%d) Attachment(0) Attachment: Auto-Detect Backup Backup a database Backup the choosen database. Body: Bottom Bug Tracker CC: CSV Parameters C_hange C_onnect C_reate Can not connect to the server!
1. Try to check if the server is running.
2. Find out on which address and port it is listening.
3. If there is a firewall between the server and this client, make sure that the server address and port (usually 8000) are not blocked.
Click on 'Change' to change the address. Can't create the database, caused by an unknown reason.
If there is a database created, it could be broken. Maybe drop this database! Please check the error message for possible informations.
Error message:
 Cancel connection to the Tryton server Change Accelerators Check for an automatic database update after restoring a database from a previous Tryton version. Choose a Plugin Choose a Tryton database to backup: Choose a Tryton database to delete: Choose a password for the admin user of the new database. With these credentials you will be later able to login into the database:
User name: admin
Password: <The password you set here> Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database to be restored.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! 
Example: tryton Choose the name of the new database.
Allowed characters are alphanumerical or _ (underscore)
You need to avoid all accents, space or special characters! Example: tryton Clear Close Tab Command Line: Compare Concurrency Exception Confirm admin password: Confirmation Connect the Tryton server Connection error!
Bad username or password! Connection error!
Unable to connect to the server! Copy selected text Could not connect to server! Could not connect to the server Create Create a new record Create new database Create new line Create new relation Create the new database. Created new bug with ID  Creation Date: Creation User: Current Password: Cut selected text Data_base Database backuped successfully! Database drop failed with error message:
 Database drop failed! Database dropped successfully! Database dump failed with error message:
 Database dump failed! Database is password protected! Database name: Database restore failed with error message:
 Database restore failed! Database restored successfully! Database: Date Selection Date Time Selection Date/Datetime Entries Shortcuts Default language: Delete Delete Export Delete a database Delete selected record Delete the choosen database. Dro_p Database... E-Mail E-Mail report Edit Files Actions Edit User Preferences Edit selected record Edition Widgets Email Email Program Settings Encoding: Error Error creating database! Error opening CSV file Error processing the file at field %s. Error trying to import this record:
%s
Error Message:
%s

%s Error:  Exception: Export to CSV False Fetching databases list Field Separator: Field name File "%s" not found File Actions File Type File _Actions... File to Import: File to Restore: Find Go to ID Go to ID: Height: Host / Database information Host: Hostname: Hour: ID: Image Size Image size too large! Images Import from CSV Importation Error! Incompatible version of the server Invalid form! It is not possible to dump a password protected Database.
Backup and restore needed to be proceed manual. It is not possible to restore a password protected database.
Backup and restore needed to be proceed manual. Keyboard Shortcuts Latest Modification Date: Latest Modification by: Launch action Left Legend of Available Placeholders: Limit Limit: Lines to Skip: Link List Entries Shortcuts Login M Manage Shortcut Mark line for deletion Minute: Missing admin password! Model: N Name Network Error! New New Database Name: New database setup: Next Next Record Next widget No No String Attr. No action defined! No available plugin for this resource! No connection! No database found, you must create one! No other language available! No record selected! Nothing to print! Open Open Backup File to Restore... Open a record Open related records Open relation Open report Open the calendar Open... Open/Search relation Operation failed!
Error message:
%s Operation in progress PNG image (*.png) Password: Passwords doesn't match! Paste copied text Please wait,
this operation may take a while... Port: Preference Preferences Previous Previous Record Previous widget Print Print Workflow Print Workflow (Complex) Print report Profile Profile Editor Profile: Record saved! Records not removed! Records removed! Relate Relation Entries Shortcuts Reload Remove Report Report Bug Reports Requests (%s/%s) Restore Restore Database Restore the database from file. Right SSL connection Save Save As Save As... Save Export Save Tree Expanded State Save Width/Height Save this record Search Search / Open a record Search Limit Settings Search Limit... Search a record Security risk! Select a File... Select an Image... Select your action Selection Server Connection: Server Setup: Server connection: Server: Setup the server connection... Show plain text Sorry, wrong password for the Tryton server. Please try again. Spell Checking Statusbar Subject: Switch Switch view Tabs Position Text Delimiter: Text Entries Shortcuts The database name is restricted to alpha-nummerical characters and "_" (underscore). Avoid all accents, space and any other special characters. The following action requires to close all tabs.
Do you want to continue? The new admin password doesn't match the confirmation field.
 The same bug was already reported by another user.
To keep you informed your username is added to the nosy-list of this issue The server fingerprint has changed since last connection!
The application will stop connecting to this server until its fingerprint is fixed. This client version is not compatible with the server! This database name already exist! This is the URL of the Tryton server. Use server 'localhost' and port '8000' if the server is installed on this computer. Click on 'Change' to change the address. This is the URL of the server. Use server 'localhost' and port '8000' if the server is installed on this computer. Click on 'Change' to change the address. This is the password of the Tryton server. It doesn't belong to a real user. This password is usually defined in the trytond configuration. This record has been modified
do you want to save it ? Tips To report bugs you must have an account on <u>%s</u> To: Too much arguments Top Translate view True Tryton Connection Tryton Server Password: Type the Admin password again Unable to set locale %s Unable to write config file %s! Undelete selected record Unknown Unmark line for deletion Update Database: Use "%s" as a placeholder for the file name User name: User: Username: View _Logs... Waiting requests: %s received - %s sent What is the name of this export? Width: Wizard Working now on the duplicated record(s)! Write Anyway Wrong Tryton Server Password
Please try again. Wrong Tryton Server Password.
Please try again. Wrong characters in database name! Wrong icon for the button! Y Yes You are going to delete a Tryton database.
Are you really sure to proceed? You can not log into the system!
Verify if you have a menu defined on your user. You can use special operators:
* + to increase the date
* - to decrease the date or clear
* = to set the date or the current date

Available variables are:
h for hours
d for days
w for weeks (only with +/-)
m for months
y for years

Examples:
"+21d" increase of 21 days the date
"=11m" set the date to the 11th month of the year
"-2w" decrease of 2 weeks the date You have to select one record! You must select a record to use the relation! You must select an import file first! You need to save the record before adding translations! Your selection: _About... _Actions... _Add _Backup Database... _Cancel _Close Tab _Connect... _Default _Delete... _Disconnect _Display a new tip next time _Duplicate _E-Mail... _Email... _Execute a Plugin _Export Data... _File _Form _Go to Record ID... _Help _Home _Icons _Import Data... _Keyboard Shortcuts... _Manage profiles _Menu Reload _Menu Toggle _Menubar _Mode _New _New Database... _Next _Normal _Options _PDA _Plugins _Preferences... _Previous _Print... _Quit... _Read my Requests _Relate... _Reload _Reload/Undo _Remove _Report... _Restore Database... _Save _Save Options _Send a Request _Shortcuts _Switch View _Text _Text and Icons _Tips... _Toolbar _User d h logging everything at INFO level m specify alternate config file specify channels to log specify the log level: INFO, DEBUG, WARNING, ERROR, CRITICAL specify the login user specify the server hostname specify the server port true w y yes Project-Id-Version: tryton 0.0.1
Report-Msgid-Bugs-To: issue_tracker@tryton.org
POT-Creation-Date: 2011-10-18 21:08+0200
PO-Revision-Date: 2011-10-18 21:09+0100
Last-Translator: Mathias Behrle <mbehrle@m9s.biz>
Language-Team: de_DE <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.6
  von  %d Datensatz importiert! %d Datensatz gespeichert! %d Datensätze importiert! %d Datensätze gespeichert! %s Datei:  : :  <b>Alle Felder</b> <b>Kennen Sie Triton, einen der Namensvettern für unser Projekt?</b>

Triton (ausgesprochen /ˈtraɪtən/ TRYE-tən, oder griechisch Τρίτων)
ist der größte Mond des Planeten Neptun, entdeckt am 10. Oktober 1846
von William Lassell. Er ist der einzige große Mond des Solarsystems mit
retrograder Umlaufbahn, d.h. einer Umlaufbahn in gegensätzlicher Richtung
zur Rotation des Planeten. Mit 2700 km im Durchmesser ist er der siebtgrößte
Mond des Solarsystems. Triton verkörpert mehr als 99,5 Prozent der
bekannten Gesamtmasse des Orbits von Neptun, die Planetenringe und zwölf
andere Monde eingeschlossen. Er ist auch massereicher als alle
159 bekannten Monde des Sonnensystems zusammen.
 <b>Graphik exportieren</b>

Graphiken können per Rechts-Klick als PNG-Datei gespeichert werden.
 <b>Export von Datensätzen in Listen</b>

Datensätze können aus Listen mit Ctrl + C kopiert werden
und in beliebige Anwendungen mit Ctrl + V eingefügt werden.
 <b>Zu exportierende Felder</b> <b>Zu importierende Felder</b> <b>Optionen</b> <b>Vordefinierte Exporte</b> <b>Willkommen auf Tryton</b>


 <b>Aktualisierungskonflikt beim Schreiben des aktuellen Datensatzes:</b>

Dieser Datensatz wurde anderweitig geändert, während Sie ihn bearbeitet haben.
  Lösungsmöglichkeiten:
   - "Abbrechen" um den Speichervorgang abzubrechen
   - "Vergleichen" um die geänderte Version des Datensatzes zu betrachten
   - "Überschreiben" um die gespeicherte Version mit Ihrer Version zu überschreiben Eine Datenbank mit diesem Namen existiert bereits.
Versuchen Sie einen anderen Datenbanknamen. Zugang verweigert! Aktion Aktionen Hinzufügen Favorit hinzufügen Übersetzung hinzufügen Feldnamen _hinzufügen Einen Anhang zum Datensatz hinzufügen Das Administrator Passwort und dessen Wiederholung werden benötigt um eine neue Datenbank für Tryton anlegen zu können. Administrator Passwort: Alle Dateien Diese Warnung künftig nicht mehr anzeigen Anwendungsfehler! Möchten Sie diesen Datensatz wirklich löschen? Möchten Sie diese Datensätze wirklich löschen? Anhänge(%d) Anhang(0) Anhang: Automatische Erkennung Sichern Datenbank sichern Die ausgewählte Datenbank sichern Textkörper: Unten Fehler melden CC: CSV Parameter _Bearbeiten _Verbinden _Erstellen Kann nicht mit dem Tryton-Server verbinden!
1. Stellen Sie fest, ob der Tryton-Server läuft.
2. Finden Sie heraus auf welcher Adresse und unter welchem Port der Server erreichbar ist.
3. Wenn sich eine Firewall zwischen dem Tryton-Server und diesem Client befindet, stellen Sie sicher, dass die Tryton-Serveradresse
und der Port (üblicherweise 8000) nicht durch die Firewall blockiert werden.
Wählen Sie 'Bearbeiten', um die Adresse des Tryton-Servers anzupassen. Kann wegen eines unbekannten Problems nicht mit dem Tryton-Server verbinden.
Wenn eine Datenbank erstellt wurde, könnte diese unvollständig oder defekt sein. Es wird empfohlen die soeben erstellte Datenbank zu löschen! Bitte beachten Sie die folgende Fehlermeldung für weitere Informationen zu diesem Fehler.
Fehlermeldung:
 Abbrechen Schnelltasten ändern Bei Aktivierung wird nach der Wiederherstellung einer Datenbank, die mit einer früheren Version von Tryton erstellt wurde, eine automatische Aktualisierung der Datenbank durchgeführt. Plugin auswählen Wählen Sie die Tryton Datenbank aus, die gesichert werden soll: Wählen Sie die Tryton Datenbank aus, die gelöscht werden soll: Wählen Sie hier ein Passwort für den Benutzer mit Administrationsrechten für die neue Datenbank. Mit diesem Passwort werden Sie sich anschließend in der neuen Datenbank anmelden können:
Anmeldename: admin
Passwort: <Das hier vergebene Passwort> Wählen Sie die Standardsprache für die neue Datenbank. Nach der Installation ist es im Administrationsmenü möglich eine neue Sprache einzustellen. Wählen Sie die Datenbank, die wiederhergestellt werden soll.
Erlaubt sind alphanumerische Zeichen und _ (Unterstrich)
Nicht erlaubt sind Akzente, Leerzeichen oder spezielle Zeichen! 
Beispiel: tryton Geben Sie hier den Namen der neuen Datenbank ein.
Erlaubt sind alle Buchstaben, Zahlen und _ (Unterstrich).
Nicht erlaubt sind Umlaute, Akzente, Leerzeichen oder Sonderzeichen wie 'ß'!
Beispielname: tryton Leeren Tab schließen Kommandozeile: Vergleichen Aktualisierungskonflikt Passwort Wiederholung: Bestätigung Verbindung zum Tryton Server herstellen Verbindungsfehler!
Anmeldename oder Passwort fehlerhaft. Verbindungsfehler!
Kann nicht zum Server verbinden. Ausgewählten Text kopieren Kann nicht zum Server verbinden! Kann nicht zum Server verbinden! Erstellen Neuen Datensatz erstellen Neue Datenbank erstellen Neue Zeile erstellen Neue Beziehung erstellen Neue Datenbank erstellen Neuer Fehlerbericht angelegt mit ID  Erstellt am Erstellt von Aktuelles Passwort: Ausgewählten Text ausschneiden _Datenbank Datenbank erfolgreich gesichert! Das Löschen der Datenbank ist fehlgeschlagen mit der folgenden Fehlermeldung:
 Datenbank konnte nicht gelöscht werden! Datenbank erfolgreich gelöscht! Die Datenbanksicherung ist fehlgeschlagen mit der Fehlermeldung:
 Datenbank konnte nicht gesichert werden! Datenbank ist passwortgeschützt! Name: Wiederherstellung der Datenbank ist fehlgeschlagen mit der Fehlermeldung:
 Wiederherstellung der Datenbank ist fehlgeschlagen! Datenbank wurde erfolgreich wiederhergestellt! Datenbank: Auswahl Datum Auswahl Zeit Schnelltasten für Datums/Zeiteinträge Standard Sprache: Löschen Export löschen Datenbank löschen Ausgewählten Datensatz löschen Die ausgewählte Datenbank löschen Datenbank _löschen... E-Mail Bericht per E-Mail senden Dateiaktionen bearbeiten Benutzereinstellungen bearbeiten Ausgewählten Datensatz bearbeiten Widgets zur Bearbeitung E-Mail Einstellungen E-Mail Kodierung: Fehler Fehler bei der Datenbankerstellung! Fehler beim öffnen der CSV-Datei Fehler bei der Verarbeitung der Datei in Feld %s. Fehler beim Import des Datensatzes:
%s
Fehlermeldung:
%s

%s Fehler: Ausnahme:  Als CSV exportieren Falsch Datenbankliste wird abgefragt... Feldtrennzeichen: Feldname Datei "%s" nicht gefunden Dateiaktionen Datei Typ Dateiaktionen... Importdatei: Datei zur Wiederherstellung: Suche Gehe zu Nr (ID) Gehe zu Nr (ID): Höhe: Host-/Datenbankdetails Host: Hostname: Stunde: ID: Bildgröße Bild ist zu groß! Bilder Aus CSV Datei importieren Fehler beim Importieren! Inkompatible Serverversion! Ungültiges Formular! Es ist nicht möglich eine passwortgeschützte Datenbank zu sichern.
Backup und Wiederherstellung müssen in diesem Fall manuell durchgeführt werden. Es ist nicht möglich eine passwortgeschützte Datenbank wiederherzustellen.
Backup und Wiederherstellung müssen in diesem Fall manuell durchgeführt werden. Schnelltasten Zuletzt verändert am Zuletzt verändert von Aktion ausführen Links Legende der verfügbaren Platzhalter: Limitierung Limit: Zu überspringende Zeilen: Verknüpfung Schnelltasten für Einträge in Listenfelder Anmeldung M Favoriten verwalten Zeile zum Löschen markieren Minute: Administrator Passwort fehlt! Modell: N Name Netzwerkfehler! Neu Name für neue Datenbank: Datenbank Einstellungen: Nächster Nächster Datensatz Nächstes Widget Nein Kein Zeichen Attr. Keine Aktion angegeben! Kein Plugin für diese Ressource vorhanden! Keine Verbindung! Keine Datenbank gefunden! Bitte eine neue Datenbank erstellen. Keine anderen Sprachen verfügbar! Kein Datensatz ausgewählt! Nichts zu drucken! Öffnen Backup Datei für Wiederherstellung öffnen... Datensatz öffnen Datensätze der Beziehung öffnen Beziehung öffnen Bericht öffnen Kalender öffnen Öffnen... Beziehung suchen/öffnen Operation fehlgeschlagen!
Fehlermeldung:
%s Vorgang wird ausgeführt PNG Bild (*.png) Passwort: Passwörter sind nicht identisch! Ausgewählten Text einfügen Bitte warten,
dieser Vorgang benötigt etwas Zeit... Port: Einstellung Einstellungen Vorheriger Vorheriger Datensatz Vorheriges Widget Drucken Workflow drucken Erweiterten Workflow drucken Bericht drucken Profil Profilbearbeitung Profil: Datensatz gespeichert! Datensätze nicht gelöscht! Datensätze gelöscht! Beziehung Schnelltasten für Einträge in verknüpften Feldern Neu laden Entfernen Bericht Fehler berichten Berichte Anfragen (%s/%s) Wiederherstellen Datenbank wiederherstellen Datenbank aus Datei wiederherstellen Rechts SSL Verbindung Speichern Speichern unter... Speichern unter... Export speichern Menüansicht speichern Breite/Höhe speichern Diesen Datensatz speichern Suchen Datensatz öffnen / suchen Einstellung Suchlimitierung Suchlimitierung... Datensatz suchen Sicherheitsrisiko! Datei auswählen... Bild auswählen... Aktion wählen Auswahl Serververbindung: Tryton Server Einstellungen: Serververbindung: Server: Serververbindung konfigurieren... Klartext anzeigen Das für den Tryton Server eingegebene Passwort scheint falsch zu sein. Bitte nochmals eingeben. Rechtschreibkorrektur Statusleiste Betreff: Ansicht wechseln Ansicht wechseln Position der Tabs Texttrennzeichen: Schnelltasten für Texteinträge Die erlaubten Zeichen des Datenbanknamens sind auf Buchstaben, Zahlen und "_" (Unterstrich) beschränkt. Akzente, Leerzeichen und andere Spezialzeichen sollten nicht benutzt werden.Umlaute, Akzente und 'ß'  sind ebenfalls verboten. Die folgende Aktion erfordert die Schließung aller Tabs.
Fortfahren? Die beiden Eingaben für das neue Administrator Passwort stimmen nicht überein.
 Der selbe Fehlerbericht wurde bereits von einem anderen Benutzer berichtet.
Zu Ihrer Information wurde Ihr Benutzername auf der Interessentenliste eingetragen. Der Fingerabdruck des Servers hat sich seit der letzten Verbindung geändert!
Die Anwendung wird sich nicht mehr zu diesem Server verbinden bis dessen Fingerabdruck korrigiert wurde. Die Version dieses Clients ist nicht mit dem Server kompatibel! Eine Datenbank mit diesem Namen existiert bereits! Hier wird die Adresse des Tryton-Servers angezeigt. Tragen Sie  für Server 'localhost' und Port '8000' ein, wenn der Server lokal auf diesem Computer installiert ist. Klicken Sie auf 'Bearbeiten' um diese Adresse zu ändern. Hier wird die Adresse des Tryton-Servers angezeigt. Tragen Sie  für Server 'localhost' und Port '8000' ein, wenn der Server lokal auf diesem Computer installiert ist. Klicken Sie auf 'Bearbeiten' um diese Adresse zu ändern. Hier wird das Administrationspasswort für den Tryton Server erwartet. Dieses Passwort ist nicht das Passwort eines Tryton Benutzers, sondern wird üblicherweise in der Konfiguration des Tryton Servers angegeben. Dieser Datensatz wurde geändert.
Soll der Datensatz gespeichert werden?  Tipps Um Fehler online berichten zu können, benötigen Sie ein Konto auf <u>%s</u> An: Zu viele Argumente Oben Aktuelle Sicht übersetzen Wahr Verbindung Tryton Server Passwort: Wiederholen Sie die Eingabe des Administrator Passworts um sicher zu gehen, dass Sie sich beim ersten Mal nicht vertippt haben. Kann locale %s nicht setzen Kann Konfigurationsdatei '%s' nicht schreiben! Löschung des ausgewählten Datensatzes rückgängig machen Unbekannt Löschmarkierung für Zeile aufheben Datenbank aktualisieren: Verwenden Sie "%s" als Platzhalter für den Dateinamen Anmeldename: Anmeldename: Anmeldename: _Protokoll ansehen... Wartende Anfragen: %s erhalten - %s gesendet Wie soll der Name des Exports lauten? Breite: Wizard Sie arbeiten nun an dem duplizierten Datensatz! Überschreiben Das Passwort für den Tryton Server ist falsch!
Bitte geben Sie es noch einmal ein. Falsches Passwort für Tryton Server.
Bitte versuchen Sie es noch einmal. Unerlaubte Zeichen im Namen für die Datenbank! Falsches Icon für diesen Knopf! J Ja Sie werden eine Tryton Datenbank löschen.
Wirklich fortfahren? Systemanmeldung fehlgeschlagen!
Überprüfen Sie bitte, ob für Ihren Benutzernamen eine Menüaktion eingestellt ist. Spezielle Operatoren:
* + erhöht das Datum
* - vermindert oder löscht das Datum
* = setzt das Datum bzw. aktuelle Datum

Verfügbare Variablen:
h für Stunden
d für Tage
w für Wochen
m für Monate
y für Jahre

Beispiele:
"+21d" erhöht das Datum um 21 Tage
"=11m" setzt das Datum auf den 11. Monat des Jahres
"-2w" vermindert das Datum um 2 Wochen Sie müssen mindestens einen Datensatz auswählen! Sie müssen zuerst einen Datensatz auswählen, um die Beziehung anzeigen zu können! Zuerst muss eine Importdatei ausgewählt werden! Datensatz muss gespeichert werden, bevor eine Übersetzung hinzugefügt werden kann! Ihre Auswahl: _Über... _Aktion ausführen... _Hinzufügen Datenbank _sichern... _Abbrechen Tab schließen _Verbinden... _Standard _Löschen... Verbindung _trennen _Tipps beim Start von Tryton anzeigen _Duplizieren _Bericht per E-Mail... _E-Mail... _Plugin ausführen Daten _exportieren... _Datei _Formular _Gehe zu Datensatz Nr... _Hilfe Sta_rtseite _Symbole Daten _importieren... _Schnelltasten... _Profile verwalten _Menü neu laden Menü _umschalten _Menüleiste _Modus _Neu _Neue Datenbank... _Nächster _Normal _Einstellungen _PDA _Plugins _Einstellungen... _Vorheriger _Bericht drucken... _Beenden... Meine Anfragen _lesen _Beziehung öffnen... _Neu laden Neu laden/_Rückgängig _Entfernen _Bericht öffnen... Datenbank _wiederherstellen... _Speichern Einstellungen _speichern Anfrage _senden Fa_voriten _Ansicht wechseln _Text Text _und Symbole _Tipps... _Werkzeugleiste _Benutzer t h Allgemeiner Log-Level: INFO m Alternative Konfigurationsdatei angeben Log-Channel angeben Log-Level angeben: INFO, DEBUG, WARNING, ERROR, CRITICAL Anmeldename angeben Name des Servers angeben Port des Servers angeben Wahr W J Ja 